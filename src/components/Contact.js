import React, { Component } from "react";
import axios from "axios";
import "./../loginStyle.css";
import { API_URL } from "../Constants";
import { FormattedMessage } from "react-intl";
class Contact extends Component {
  constructor(props) {
    super(props);
    this.state = {
      successfullAlert: "Message is successfully sent.",
      errorAlert: "An error occured.",
      email: "",
      name: "",
      message: ""
    };
  }

  componentDidMount() {
    try {
      var successfullAlert = JSON.parse(localStorage.getItem("language"))
        .messages["contact.successfullAlert"];
      var errorAlert = JSON.parse(localStorage.getItem("language")).messages[
        "contact.errorAlert"
      ];

      this.setState({
        successfullAlert: successfullAlert,
        errorAlert: errorAlert
      });
    } catch (error) {}
  }

  handleName = event => {
    this.setState({
      name: event.target.value
    });
  };

  handleEmail = event => {
    this.setState({
      email: event.target.value
    });
  };

  handleMessage = event => {
    this.setState({
      message: event.target.value
    });
  };

  submitHandler = event => {
    event.preventDefault();

    axios
      .post(`${API_URL}contact`, this.state, { timeout: 10000 })
      .then(Response => {
        window.alert(this.state.successfullAlert);
      })
      .catch(error => {
        window.alert(this.state.errorAlert);

        if (error.response) {
        }
      });
  };

  render() {
    return (
      <div className="form-container">
        <div className="form">
          <form onSubmit={this.submitHandler}>
            <div className="user-logo-img">
              <img src="/contact-email-img.png" width="100%" />
            </div>
            <div className="form-name">
              <span>
                <h2>Contact</h2>
              </span>
            </div>
            <div className="input-name">
              <FormattedMessage id="contact.name" defaultMessage="Name" />:
            </div>
            <div>
              <input
                required
                type="text"
                value={this.state.name}
                onChange={this.handleName}
                autocomplete="off"
              />
            </div>

            <div className="input-name">
              <FormattedMessage id="user.email" defaultMessage="Email" />:
            </div>
            <div>
              <input
                required
                type="text"
                autocomplete="off"
                value={this.state.email}
                onChange={this.handleEmail}
              />
            </div>
            <div className="input-name">
              <FormattedMessage id="contact.message" defaultMessage="Message" />
              :
            </div>
            <div>
              <textarea
                required
                cols="23"
                rows="10"
                autocomplete="off"
                value={this.state.message}
                onChange={this.handleMessage}
              />
            </div>
            <div></div>
            <div>
              <button className="form-btn" type="submit">
                <FormattedMessage id="contact.submit" defaultMessage="Submit" />
              </button>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default Contact;
