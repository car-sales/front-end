import React, { Component } from "react";
import axios from "axios";
import { API_URL } from "../Constants";
import SwiftSlider from "react-swift-slider";
import { getCookie } from "../CookieManager";
import Lightbox from "react-image-lightbox";
import "react-image-lightbox/style.css";
import { FormattedMessage } from "react-intl";
import { toLowerCaseV2 } from "../Util";
import Loader from "./Loader";

class AdPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      fullSizeImages: [],
      fullSizeIsOpen: false,
      photoIndex: 0,
      ad: {},
      views: null,
      todayViews: null,
      images: [],
      fav_icons: ["far fa-star", "fas fa-star"],
      active_fav_icon: 0,
      isAdLiked: false,
      displayFavIcon: false,
    };
  }

  componentDidMount() {
    const {
      match: { params },
    } = this.props;

    axios
      .get(`${API_URL}ad/${params.id}`)
      .then((Response) => {
        var ad = Response.data;
        var description = ad.description.split("\n");

        ad.description = description;

        var currentUser = JSON.parse(localStorage.getItem("user"));
        if (currentUser !== null) {
          var usersAd = false;

          currentUser.userAdsDto.map((ad) => {
            if (params.id == ad.id) {
              usersAd = true;
            }
          });

          if (!usersAd) {
            var likedAd = false;
            currentUser.likedAdsDto.map((ad) => {
              if (params.id == ad.id) {
                likedAd = true;
              }
            });

            this.setState({
              active_fav_icon: likedAd ? 1 : 0,
              isAdLiked: likedAd,
              displayFavIcon: true,
            });
          } else {
            this.setState({ displayFavIcon: false });
          }
        } else {
          this.setState({
            active_fav_icon: 0,
            isAdLiked: false,
            displayFavIcon: true,
          });
        }

        this.setState({
          ad: ad,
          images: Response.data.images,
        });
      })
      .catch((error) => {
        if (error.response) {
        }
      });

    axios
      .get(`${API_URL}ads/${params.id}/views`)
      .then((Response) => {
        this.setState({
          views: Response.data.views,
          todayViews: Response.data.todayViews,
        });
      })
      .catch((error) => {
        console.log(error.response.data);
      });

    axios
      .get(`${API_URL}ad/${params.id}/bigImages`)
      .then((Response) => {
        this.setState({
          fullSizeImages: Response.data,
        });
      })
      .catch((error) => {});
  }

  favoriteClick = (event) => {
    if (getCookie("access_token") != null) {
      if (!this.state.isAdLiked) {
        this.setState(
          {
            isAdLiked: true,
            active_fav_icon: 1,
          },
          function () {
            axios
              .get(`${API_URL}like/ad/${this.props.match.params.id}`, {
                params: { token: getCookie("access_token") },
                headers: {
                  Authorization: getCookie("access_token"),
                },
              })
              .then((Response) => {
                axios
                  .get(`${API_URL}current-user`, {
                    params: { token: getCookie("access_token") },
                    headers: {
                      Authorization: getCookie("access_token"),
                    },
                  })
                  .then((Response) => {
                    const userData = Response.data;
                    // localStorage.removeItem("user");
                    localStorage.setItem("user", JSON.stringify(userData));
                  })
                  .catch((error) => {
                    if (error.response) {
                      console.log(error.response);
                    }
                  });
              })
              .catch((error) => {
                if (error.response) {
                  console.log(error.response);
                }
              });
          }
        );
      } else {
        this.setState(
          {
            isAdLiked: false,
            active_fav_icon: 0,
          },
          function () {
            axios
              .get(`${API_URL}unlike/ad/${this.props.match.params.id}`, {
                params: { token: getCookie("access_token") },
                headers: {
                  Authorization: getCookie("access_token"),
                },
              })
              .then((Response) => {
                axios
                  .get(`${API_URL}current-user`, {
                    params: { token: getCookie("access_token") },
                    headers: {
                      Authorization: getCookie("access_token"),
                    },
                  })
                  .then((Response) => {
                    const userData = Response.data;
                    // localStorage.removeItem("user");
                    localStorage.setItem("user", JSON.stringify(userData));
                  })
                  .catch((error) => {
                    if (error.response) {
                    }
                  });
              })
              .catch((error) => {
                if (error.response) {
                }
              });
          }
        );
      }
    } else {
      window.location.href = "/login";
    }
  };

  openImagePopUp = (event) => {
    event.preventDefault();
    this.setState({
      fullSizeIsOpen: !this.state.fullSizeIsOpen,
      photoIndex: parseInt(event.target.name),
    });
  };

  render() {
    var imgKey = 0;
    const images = this.state.images;
    const images2 = [];
    const ad = this.state.ad;
    const data = [];
    if (images.length > 0) {
      for (var i = 0; i < images.length; i++) {
        data.push({ id: i + 1, src: "data:image/png;base64," + images[i] });
      }
    }

    if (this.state.fullSizeImages.length > 0) {
      for (var i = 0; i < images.length; i++) {
        images2.push("data:image/png;base64," + this.state.fullSizeImages[i]);
      }
    }

    return (
      <div>
        {ad.id ? (
          <div className="ad-content">
            {this.state.fullSizeIsOpen ? (
              <Lightbox
                mainSrcThumbnail={images2[this.state.photoIndex]}
                nextSrcThumbnail={
                  images2[(this.state.photoIndex + 1) % images2.length]
                }
                prevSrcThumbnail={
                  images2[
                    (this.state.photoIndex + images2.length - 1) %
                      images2.length
                  ]
                }
                mainSrc={images2[this.state.photoIndex]}
                nextSrc={images2[(this.state.photoIndex + 1) % images2.length]}
                prevSrc={
                  images2[
                    (this.state.photoIndex + images2.length - 1) %
                      images2.length
                  ]
                }
                onCloseRequest={() => this.setState({ fullSizeIsOpen: false })}
                onMovePrevRequest={() =>
                  this.setState({
                    photoIndex:
                      (this.state.photoIndex + images2.length - 1) %
                      images2.length,
                  })
                }
                onMoveNextRequest={() =>
                  this.setState({
                    photoIndex: (this.state.photoIndex + 1) % images2.length,
                  })
                }
              />
            ) : (
              ""
            )}

            <div className="ad-header">
              <div className="ad-user">
                <span className="ad-username">
                  <i class="fas fa-user"></i> &nbsp;
                  {this.state.ad.user.userName}
                </span>
              </div>
              <div className="ad-date">
                <span>{this.getDate(ad.createdAt)}</span>
              </div>

              {!this.state.displayFavIcon ? (
                ""
              ) : (
                <div className="fav-img-div">
                  <span
                    className="fav-icon"
                    title={
                      this.state.active_fav_icon == 0
                        ? "Save to favorites"
                        : "Delete from favorites"
                    }
                    onClick={this.favoriteClick}
                  >
                    <i
                      class={this.state.fav_icons[this.state.active_fav_icon]}
                    ></i>
                  </span>
                </div>
              )}
              <div>
                <span className="views-text">
                  &nbsp;
                  <i class="fas fa-eye"></i>
                  &nbsp;
                  {this.state.views === null ? (
                    <i className="fas fa-ellipsis-h" />
                  ) : (
                    this.state.views
                  )}
                  &nbsp; (
                  <FormattedMessage
                    id="ad.today"
                    defaultMessage="today"
                  />:{" "}
                  {this.state.todayViews === null ? (
                    <i className="fas fa-ellipsis-h" />
                  ) : (
                    this.state.todayViews
                  )}
                  )
                </span>
              </div>
            </div>
            <div className="image-slider">
              <span
                className="btn-fullscreen"
                onClick={() => {
                  this.setState({
                    fullSizeIsOpen: true,
                    photoIndex: 0,
                  });
                }}
              >
                Fullscreen
              </span>
              <SwiftSlider data={data} interval={10000} />
            </div>
            <br />
            <div className="ad-images">
              {images.map((img) => (
                <div key={imgKey} className="img">
                  <img
                    title="Open image"
                    name={imgKey++}
                    onClick={this.openImagePopUp}
                    src={`data:image/png;base64,${img}`}
                    width="100%"
                  />
                </div>
              ))}
            </div>
            <br />
            <br />
            {this.state.ad.description.length > 1 ? (
              <div className="ad-description">
                <span>{this.displayDescription()}</span>
              </div>
            ) : (
              ""
            )}
            <div className="properties">
              <div className="ad-properties">
                <div className="property-name">
                  <div className="properties-img">
                    <img src="/ad-icon.png" width="60%" />
                  </div>
                  <FormattedMessage id="ad" defaultMessage="Ad" />
                </div>
                <div className="sub-properties">
                  <div className="sub-property-name">
                    <FormattedMessage
                      id="home.filter.dealType"
                      defaultMessage="Deal type"
                    />
                    :
                  </div>
                  <div className="sub-property-value">
                    <FormattedMessage
                      id={"home.filter.dealType." + toLowerCaseV2(ad.dealType)}
                      defaultMessage={ad.dealType}
                    />
                  </div>
                  <div className="sub-property-name">
                    <FormattedMessage
                      id="home.filter.price"
                      defaultMessage="Price"
                    />
                    :
                  </div>
                  <div className="sub-property-value">
                    {ad.price == -1 ? (
                      <FormattedMessage
                        id="home.ads.negotiable"
                        defaultMessage="Negotiable"
                      />
                    ) : (
                      ad.price + " " + ad.currency
                    )}
                  </div>
                  <div className="sub-property-name">
                    <FormattedMessage
                      id="home.filter.countries"
                      defaultMessage="Country"
                    />
                    :
                  </div>
                  <div className="sub-property-value">{ad.region.country}</div>
                  <div className="sub-property-name">
                    <FormattedMessage
                      id="home.filter.region"
                      defaultMessage="Region"
                    />
                    :
                  </div>
                  <div className="sub-property-value">
                    {ad.region.regionName}
                  </div>
                  <div className="sub-property-name">
                    <FormattedMessage id="user.email" defaultMessage="Email" />:
                  </div>
                  <div className="sub-property-value">{ad.email}</div>{" "}
                  <div className="sub-property-name">
                    <FormattedMessage id="user.phone" defaultMessage="Phone" />:
                  </div>
                  <div className="sub-property-value">{ad.phone}</div>
                </div>
              </div>
              <div className="car-properties">
                <div className="car-properties">
                  <div className="property-name">
                    <div className="properties-img">
                      <img
                        className="car-props-img"
                        src="/car-icon.png"
                        width="100%"
                      />
                    </div>
                    <span>
                      <FormattedMessage id="car" defaultMessage="Car" />
                    </span>
                  </div>
                  <div className="sub-properties">
                    <div className="sub-property-name">
                      <FormattedMessage
                        id="home.filter.brands"
                        defaultMessage="Brand"
                      />
                      :
                    </div>
                    <div className="sub-property-value">
                      {ad.car.model.brand}
                    </div>
                    <div className="sub-property-name">
                      <FormattedMessage id="ad.model" defaultMessage="Model" />:
                    </div>
                    <div className="sub-property-value">
                      {ad.car.model.modelName}
                    </div>
                    <div className="sub-property-name">
                      <FormattedMessage
                        id="home.filter.state"
                        defaultMessage="State"
                      />
                      :
                    </div>
                    <div className="sub-property-value">
                      <FormattedMessage
                        id={"home.filter.state." + toLowerCaseV2(ad.car.state)}
                        defaultMessage={ad.car.state}
                      />
                    </div>
                    <div className="sub-property-name">
                      <FormattedMessage
                        id="home.filter.steerWheels"
                        defaultMessage="Steer wheel"
                      />
                      :
                    </div>
                    <div className="sub-property-value">
                      <FormattedMessage
                        id={
                          "home.filter.steerWheels." +
                          toLowerCaseV2(ad.car.steerWheelSide)
                        }
                        defaultMessage={ad.car.steerWheelSide}
                      />
                    </div>
                    <div className="sub-property-name">
                      <FormattedMessage id="ad.doors" defaultMessage="Doors" />:
                    </div>
                    <div className="sub-property-value">
                      {ad.car.doorsNumber}
                    </div>
                    <div className="sub-property-name">
                      <FormattedMessage
                        id="home.filter.mileage"
                        defaultMessage="Mileage"
                      />
                      :
                    </div>
                    <div className="sub-property-value">
                      {this.computeMileage(ad.car.mileage.toString())}
                    </div>
                    <div className="sub-property-name">
                      <FormattedMessage
                        id="home.filter.year"
                        defaultMessage="Year"
                      />
                      :
                    </div>
                    <div className="sub-property-value">{ad.car.year}</div>
                    <div className="sub-property-name">
                      <FormattedMessage
                        id="home.filter.tractions"
                        defaultMessage="Traction"
                      />
                      :
                    </div>
                    <div className="sub-property-value">
                      <FormattedMessage
                        id={
                          "home.filter.tractions." +
                          toLowerCaseV2(ad.car.traction)
                        }
                        defaultMessage={ad.car.traction}
                      />
                    </div>
                    <div className="sub-property-name">
                      <FormattedMessage
                        id="home.filter.transmissions"
                        defaultMessage="Transmission"
                      />
                      :
                    </div>
                    <div className="sub-property-value">
                      <FormattedMessage
                        id={
                          "home.filter.transmissions." +
                          toLowerCaseV2(ad.car.transmission)
                        }
                        defaultMessage={ad.car.transmissions}
                      />
                    </div>
                    <div className="sub-property-name">
                      <FormattedMessage
                        id="home.filter.bodyStyles"
                        defaultMessage="Body style"
                      />
                      :
                    </div>
                    <div className="sub-property-value">
                      <FormattedMessage
                        id={
                          "home.filter.bodyStyles." +
                          toLowerCaseV2(ad.car.bodyStyle)
                        }
                        defaultMessage={ad.car.bodyStyle}
                      />
                    </div>
                    <div className="sub-property-name">
                      <FormattedMessage
                        id="ad.exteriorColor"
                        defaultMessage="Exterior color"
                      />
                      :
                    </div>
                    <div className="sub-property-value">
                      <FormattedMessage
                        id={
                          "home.filter.colors." +
                          toLowerCaseV2(ad.car.exteriorColor)
                        }
                        defaultMessage={ad.car.exteriorColor}
                      />
                    </div>
                    <div className="sub-property-name">
                      <FormattedMessage
                        id="ad.interiorColor"
                        defaultMessage="Interior colors"
                      />
                      :
                    </div>
                    <div className="sub-property-value">
                      <FormattedMessage
                        id={
                          "home.filter.colors." +
                          toLowerCaseV2(ad.car.interiorColor)
                        }
                        defaultMessage={ad.car.interiorColor}
                      />
                    </div>
                  </div>
                  <div className="engine-properties">
                    <div className="property-name">
                      <div className="properties-img">
                        <img src="/engine-icon.png" width="70%" />
                      </div>
                      <span>
                        <FormattedMessage id="engine" defaultMessage="Engine" />
                      </span>
                    </div>
                    <div className="sub-properties">
                      <div className="sub-property-name">
                        <FormattedMessage
                          id="home.filter.fuels"
                          defaultMessage="Fuel"
                        />
                        :
                      </div>
                      <div className="sub-property-value">
                        <FormattedMessage
                          id={
                            "home.filter.fuels." +
                            toLowerCaseV2(ad.car.engine.fuel)
                          }
                          defaultMessage={ad.car.engine.fuel}
                        />
                      </div>
                      <div className="sub-property-name">
                        <FormattedMessage
                          id="home.filter.capacity"
                          defaultMessage="Capacity"
                        />
                        :
                      </div>
                      <div className="sub-property-value">
                        {ad.car.engine.capacity}{" "}
                        <FormattedMessage
                          id="home.filter.capacity.liters"
                          defaultMessage="liters"
                        />
                        .
                      </div>
                      <div className="sub-property-name">
                        <FormattedMessage
                          id="home.filter.horsePower"
                          defaultMessage="Horse power"
                        />
                        :
                      </div>
                      <div className="sub-property-value">
                        {ad.car.engine.horsePower}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="security-properties">
                <div className="property-name">
                  <div className="properties-img">
                    <img src="/securities-icon.png" width="50%" />
                  </div>
                  <span>
                    <FormattedMessage
                      id="home.filter.securities"
                      defaultMessage="Securities"
                    />
                  </span>
                </div>
                <div className="features">
                  {this.getSecurities(ad.car.securities)}
                </div>
                <div className="comfort-properties">
                  <div className="property-name">
                    <div className="properties-img">
                      <img src="/comforts-icon.png" width="50%" />
                    </div>
                    <span>
                      <FormattedMessage
                        id="home.filter.comforts"
                        defaultMessage="Comforts"
                      />
                    </span>
                  </div>
                  <div className="features">
                    {this.getComforts(ad.car.comforts)}
                  </div>
                </div>
              </div>
            </div>
          </div>
        ) : (
          <Loader classes="active second" />
        )}
      </div>
    );
  }

  displayDescription() {
    var t = [];
    var i = 0;
    this.state.ad.description.map((d) => {
      {
        t.push(d);
        t.push(<br key={++i} />);
      }
    });
    return t;
  }

  getDate(date) {
    if (date != null) {
      var t = date.split(/[- :]/);

      return (
        t[2].toString().substring(0, t[2].toString().length - 3) +
        "." +
        t[1] +
        "." +
        t[0]
      );
    }
    return "";
  }

  computeMileage(m) {
    if (m.length > 3) {
      return (
        m.substring(0, m.length - 3) + " " + m.substring(m.length - 3, m.length)
      );
    } else return m;
  }

  getComforts(comforts) {
    var c = [];

    comforts.map((comfort) => {
      c.push(
        <div className="sub-property-value">
          <FormattedMessage
            id={"home.filter.comforts." + toLowerCaseV2(comfort.comfortName)}
            defaultMessage={comfort.comfortName}
          />
        </div>
      );
    });

    return c;
  }
  getSecurities(securities) {
    var s = [];
    securities.map((security) => {
      s.push(
        <div className="sub-property-value">
          <FormattedMessage
            id={"home.filter.securities." + toLowerCaseV2(security)}
            defaultMessage={security}
          />
        </div>
      );
    });

    return s;
  }
}

export default AdPage;
