import React, { Component } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import Pagination from "react-js-pagination";
import "./../pagination.css";
import Filter from "./filters/Filter";
import { API_URL } from "../Constants";
import { get } from "../GetRequest";
import filtersObj from "../FiltersObj";
import { getCookie } from "../CookieManager";
import { FormattedMessage } from "react-intl";
import Ads from "./Ads";
import { connect, useDispatch, useSelector } from "react-redux";
import { fetchAds } from "./../redux/actions";
import Loader from "./Loader";
import $ from "jquery";

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentUser: null,
      filtersObj: null,
      isLoaded: false,
      activePage: 1,
      fuelsParam: "",
      bodyStylesParam: "",
      brandsParam: "",
      modelsParam: "",
      priceStartParam: "",
      priceEndParam: "",
      yearStartParam: "",
      yearEndParam: "",
      currencyParam: "",
      negotiableParam: "",
      steerWheelSidesParam: "",
      comfortsParam: "",
      securitiesParam: "",
      capacityStartParam: "",
      capacityEndParam: "",
      horsePowerStartParam: "",
      horsePowerEndParam: "",
      mileageStartParam: "",
      mileageEndParam: "",
      statesParam: "",
      dealTypesParam: "",
      countriesParam: "",
      regionsParam: "",
    };
  }

  getWithParams() {
    this.props.fetchAds(
      `ads?size=30&page=${
        this.state.activePage ? this.state.activePage - 1 : 0
      }` +
        this.state.priceStartParam +
        this.state.priceEndParam +
        this.state.currencyParam +
        this.state.negotiableParam +
        this.state.fuelsParam +
        this.state.bodyStylesParam +
        this.state.brandsParam +
        this.state.modelsParam +
        this.state.tractionsParam +
        this.state.transmissionsParam +
        this.state.interiorColorsParam +
        this.state.exteriorColorsParam +
        this.state.doorsNumbersParam +
        this.state.steerWheelSidesParam +
        this.state.comfortsParam +
        this.state.securitiesParam +
        this.state.yearStartParam +
        this.state.yearEndParam +
        this.state.capacityStartParam +
        this.state.capacityEndParam +
        this.state.horsePowerStartParam +
        this.state.horsePowerEndParam +
        this.state.mileageStartParam +
        this.state.mileageEndParam +
        this.state.statesParam +
        this.state.dealTypesParam +
        this.state.countriesParam +
        this.state.regionsParam
    );

    // get(
    //   `ads?size=24&page=${this.state.activePage - 1}` +
    //     this.state.priceStartParam +
    //     this.state.priceEndParam +
    //     this.state.currencyParam +
    //     this.state.negotiableParam +
    //     this.state.fuelsParam +
    //     this.state.bodyStylesParam +
    //     this.state.brandsParam +
    //     this.state.modelsParam +
    //     this.state.tractionsParam +
    //     this.state.transmissionsParam +
    //     this.state.interiorColorsParam +
    //     this.state.exteriorColorsParam +
    //     this.state.doorsNumbersParam +
    //     this.state.steerWheelSidesParam +
    //     this.state.comfortsParam +
    //     this.state.securitiesParam +
    //     this.state.yearStartParam +
    //     this.state.yearEndParam +
    //     this.state.capacityStartParam +
    //     this.state.capacityEndParam +
    //     this.state.horsePowerStartParam +
    //     this.state.horsePowerEndParam +
    //     this.state.mileageStartParam +
    //     this.state.mileageEndParam +
    //     this.state.statesParam +
    //     this.state.dealTypesParam +
    //     this.state.countriesParam +
    //     this.state.regionsParam
    // )
    //   .then(Response => {
    //     this.setState({
    //       ads: Response.data,
    //       isLoaded: true
    //     });
    //   })
    //   .catch(error => {
    //     if (error.response) {
    //     }
    //   });
  }

  updateState(stateValue, stateName) {
    this.setState(
      { [stateName]: stateValue, activePage: filtersObj.page },
      function () {
        if (stateName === "securitiesParam" || filtersObj.isFiltered === true) {
          this.getWithParams();
          filtersObj.isFiltered = false;
        }
      }
    );
  }

  fuelsFilter = (filter) => {
    var fuelsParam = "";
    if (filter.fuels.length > 0) {
      fuelsParam = "&fuels=";
      for (let i = 0; i < filter.fuels.length - 1; i++) {
        fuelsParam += filter.fuels[i] + ",";
      }
      fuelsParam += filter.fuels[filter.fuels.length - 1];
    }

    this.updateState(fuelsParam, "fuelsParam");
  };

  bodyStylesFilter = (filter) => {
    var bodyStylesParam = "";
    if (filter.bodyStyles.length > 0) {
      bodyStylesParam = "&bodyStyles=";
      for (let i = 0; i < filter.bodyStyles.length - 1; i++) {
        bodyStylesParam += filter.bodyStyles[i] + ",";
      }
      bodyStylesParam += filter.bodyStyles[filter.bodyStyles.length - 1];
    }

    this.updateState(bodyStylesParam, "bodyStylesParam");
  };

  brandsFilter = (filter) => {
    var brandsParam = "";
    if (filter.brands.length > 0) {
      brandsParam = "&brands=";
      for (let i = 0; i < filter.brands.length - 1; i++) {
        brandsParam += filter.brands[i].replace("_", " ") + ",";
      }
      brandsParam += filter.brands[filter.brands.length - 1].replace("_", " ");
    }
    this.updateState(brandsParam, "brandsParam");
  };

  modelsFilter = (filter) => {
    var modelsParam = "";
    if (filter.models.length > 0) {
      modelsParam = "&models=";
      for (let i = 0; i < filter.models.length - 1; i++) {
        modelsParam += filter.models[i].replace("_", " ") + ",";
      }
      modelsParam += filter.models[filter.models.length - 1].replace("_", " ");
    }

    this.updateState(modelsParam, "modelsParam");
  };

  countriesFilter = (filter) => {
    var countriesParam = "";
    if (filter.countries.length > 0) {
      countriesParam = "&countries=";
      for (let i = 0; i < filter.countries.length - 1; i++) {
        countriesParam += filter.countries[i].replace("_", " ") + ",";
      }
      countriesParam += filter.countries[filter.countries.length - 1].replace(
        "_",
        " "
      );
    }
    this.updateState(countriesParam, "countriesParam");
  };

  regionsFilter = (filter) => {
    var regionsParam = "";
    if (filter.regions.length > 0) {
      regionsParam = "&regions=";
      for (let i = 0; i < filter.regions.length - 1; i++) {
        regionsParam += filter.regions[i].replace("_", " ") + ",";
      }
      regionsParam += filter.regions[filter.regions.length - 1].replace(
        "_",
        " "
      );
    }

    this.updateState(regionsParam, "regionsParam");
  };

  tractionsFilter = (filter) => {
    var tractionsParam = "";
    if (filter.tractions.length > 0) {
      tractionsParam = "&traction=";
      for (let i = 0; i < filter.tractions.length - 1; i++) {
        tractionsParam += filter.tractions[i].replace("_", " ") + ",";
      }
      tractionsParam += filter.tractions[filter.tractions.length - 1].replace(
        "_",
        " "
      );
    }

    this.updateState(tractionsParam, "tractionsParam");
  };

  transmissionsFilter = (filter) => {
    var transmissionsParam = "";
    if (filter.transmissions.length > 0) {
      transmissionsParam = "&transmissions=";
      for (let i = 0; i < filter.transmissions.length - 1; i++) {
        transmissionsParam += filter.transmissions[i] + ",";
      }
      transmissionsParam += filter.transmissions[
        filter.transmissions.length - 1
      ].replace("-", "_");
    }

    this.updateState(transmissionsParam, "transmissionsParam");
  };

  interiorColorsFilter = (filter) => {
    var interiorColorsParam = "";
    if (filter.interiorColors.length > 0) {
      interiorColorsParam = "&interiorColors=";
      for (let i = 0; i < filter.interiorColors.length - 1; i++) {
        interiorColorsParam += filter.interiorColors[i].replace("_", " ") + ",";
      }
      interiorColorsParam += filter.interiorColors[
        filter.interiorColors.length - 1
      ].replace("_", " ");
    }

    this.updateState(interiorColorsParam, "interiorColorsParam");
  };

  exteriorColorsFilter = (filter) => {
    var exteriorColorsParam = "";
    if (filter.exteriorColors.length > 0) {
      exteriorColorsParam = "&exteriorColors=";
      for (let i = 0; i < filter.exteriorColors.length - 1; i++) {
        exteriorColorsParam += filter.exteriorColors[i].replace("_", " ") + ",";
      }
      exteriorColorsParam += filter.exteriorColors[
        filter.exteriorColors.length - 1
      ].replace("_", " ");
    }

    this.updateState(exteriorColorsParam, "exteriorColorsParam");
  };

  steerWheelSidesFilter = (filter) => {
    var steerWheelSidesParam = "";
    if (filter.steerWheelSides.length > 0) {
      steerWheelSidesParam = "&steerWheelSides=";
      for (let i = 0; i < filter.steerWheelSides.length - 1; i++) {
        steerWheelSidesParam += filter.steerWheelSides[i] + ",";
      }
      steerWheelSidesParam +=
        filter.steerWheelSides[filter.steerWheelSides.length - 1];
    }

    this.updateState(steerWheelSidesParam, "steerWheelSidesParam");
  };

  statesFilter = (filter) => {
    var statesParam = "";
    if (filter.states.length > 0) {
      statesParam = "&states=";
      for (let i = 0; i < filter.states.length - 1; i++) {
        statesParam += filter.states[i] + ",";
      }
      statesParam += filter.states[filter.states.length - 1];
    }

    this.updateState(statesParam.replace(" ", "_"), "statesParam");
  };

  doorsNumbersFilter = (filter) => {
    var doorsNumbersParam = "";
    if (filter.doorsNumbers.length > 0) {
      doorsNumbersParam = "&doorsNumber=";
      for (let i = 0; i < filter.doorsNumbers.length - 1; i++) {
        doorsNumbersParam += filter.doorsNumbers[i] + ",";
      }
      doorsNumbersParam += filter.doorsNumbers[filter.doorsNumbers.length - 1];
    }

    this.updateState(doorsNumbersParam, "doorsNumbersParam");
  };

  priceFilter = (filter) => {
    var priceStartParam = "&priceStart=" + filter.priceStart;
    var priceEndParam = "&priceEnd=" + filter.priceEnd;
    var negotiableParam = "&negotiable=" + filter.negotiable;

    this.updateState(negotiableParam, "negotiableParam");

    this.updateState(priceStartParam, "priceStartParam");
    this.updateState(priceEndParam, "priceEndParam");
    this.updateState("&currency=" + filter.currency, "currencyParam");
  };

  yearFilter = (filter) => {
    var yearStartParam = "&yearStart=" + filter.yearStart;
    var yearEndParam = "&yearEnd=" + filter.yearEnd;

    this.updateState(yearStartParam, "yearStartParam");
    this.updateState(yearEndParam, "yearEndParam");
  };

  capacityFilter = (filter) => {
    var capacityStartParam = "&capacityStart=" + filter.capacityStart;
    var capacityEndParam = "&capacityEnd=" + filter.capacityEnd;

    this.updateState(capacityStartParam, "capacityStartParam");
    this.updateState(capacityEndParam, "capacityEndParam");
  };

  horsePowerFilter = (filter) => {
    var horsePowerStartParam = "&horsePowerStart=" + filter.horsePowerStart;
    var horsePowerEndParam = "&horsePowerEnd=" + filter.horsePowerEnd;

    this.updateState(horsePowerStartParam, "horsePowerStartParam");
    this.updateState(horsePowerEndParam, "horsePowerEndParam");
  };

  mileageFilter = (filter) => {
    var mileageStartParam = "&mileageStart=" + filter.mileageStart;
    var mileageEndParam = "&mileageEnd=" + filter.mileageEnd;

    this.updateState(mileageStartParam, "mileageStartParam");
    this.updateState(mileageEndParam, "mileageEndParam");
  };

  dealTypesFilter = (filter) => {
    var dealTypesParam = "";
    if (filter.dealTypes.length > 0) {
      dealTypesParam = "&dealTypes=";
      for (let i = 0; i < filter.dealTypes.length - 1; i++) {
        dealTypesParam += filter.dealTypes[i] + ",";
      }
      dealTypesParam += filter.dealTypes[filter.dealTypes.length - 1];
    }

    this.updateState(dealTypesParam, "dealTypesParam");
  };

  comfortsFilter = (filter) => {
    var comfortsParam = "";
    if (filter.comforts.length > 0) {
      comfortsParam = "&comforts=";
      for (let i = 0; i < filter.comforts.length - 1; i++) {
        comfortsParam += filter.comforts[i].replace("_", " ") + ",";
      }
      comfortsParam += filter.comforts[filter.comforts.length - 1].replace(
        "_",
        " "
      );
    }

    this.updateState(comfortsParam, "comfortsParam");
  };

  securitiesFilter = (filter) => {
    var securitiesParam = "";
    if (filter.securities.length > 0) {
      securitiesParam = "&securities=";
      for (let i = 0; i < filter.securities.length - 1; i++) {
        securitiesParam += filter.securities[i].replace("_", " ") + ",";
      }
      securitiesParam += filter.securities[
        filter.securities.length - 1
      ].replace("_", " ");
    }
    this.updateState(securitiesParam, "securitiesParam");
  };

  handlePageChange(pageNumber) {
    if (pageNumber !== this.state.activePage) {
      filtersObj.page = pageNumber;
      console.log(filtersObj);
      this.setState({ activePage: pageNumber, ads: {} }, function () {
        this.getWithParams();
      });
    }
  }

  componentDidMount() {
    // filtersObj.isFiltered = false;

    if (getCookie("access_token") != null) {
      axios
        .get(`${API_URL}current-user`, {
          headers: { Authorization: getCookie("access_token") },
        })
        .then((Response) => {
          this.setState({
            currentUser: Response.data,
          });
        })
        .catch((error) => {
          if (error.response) {
          }
        });
    }
  }

  render() {
    const ads = this.props.state.ads;
    const loader = this.props.state.app.loading;

    return (
      <div className="ads-container">
        {loader != true || ads.adsAddedToday >= 0 ? (
          <div
            className={"ads-content " + (loader === true ? "refreshing" : "")}
          >
            {ads.adsAddedToday >= 0 ? (
              <div className="ads-content-header">
                <div>
                  <span
                    onClick={() => {
                      getCookie("access_token") == null
                        ? (window.location.href = "/login")
                        : (window.location.href = "/ad");
                    }}
                    className="ad-btn"
                  >
                    <FormattedMessage
                      id="home.add-btn"
                      defaultMessage="Submit an ad"
                    />
                  </span>
                </div>
                <div className="statistics">
                  <div>
                    <span>
                      <i class="fas fa-chart-bar"></i>
                      &nbsp;
                      <FormattedMessage
                        id="home.ads-added"
                        defaultMessage={"Ads added today: {adsCount}"}
                        values={{ adsCount: ads.adsAddedToday }}
                      />
                    </span>
                  </div>
                </div>
              </div>
            ) : (
              ""
            )}
            <div
              style={{
                visibility: ads.adsAddedToday >= 0 ? "visible" : "hidden",
              }}
              className="filter-btn"
              title="Open filters"
              onClick={() => {
                $(".filters").toggleClass("active");
              }}
            >
              <span>Filters</span>
              <i className="fas fa-filter"></i>
            </div>
            <br /> <br />
            {this.adsContent(ads)}
          </div>
        ) : (
          <Loader classes="active second" />
        )}

        <Filter
          dealTypesFilter={this.dealTypesFilter}
          priceFilter={this.priceFilter}
          yearFilter={this.yearFilter}
          capacityFilter={this.capacityFilter}
          horsePowerFilter={this.horsePowerFilter}
          fuelsFilter={this.fuelsFilter}
          mileageFilter={this.mileageFilter}
          bodyStylesFilter={this.bodyStylesFilter}
          brandsFilter={this.brandsFilter}
          modelsFilter={this.modelsFilter}
          tractionsFilter={this.tractionsFilter}
          transmissionsFilter={this.transmissionsFilter}
          interiorColorsFilter={this.interiorColorsFilter}
          exteriorColorsFilter={this.exteriorColorsFilter}
          doorsNumbersFilter={this.doorsNumbersFilter}
          steerWheelSidesFilter={this.steerWheelSidesFilter}
          comfortsFilter={this.comfortsFilter}
          securitiesFilter={this.securitiesFilter}
          statesFilter={this.statesFilter}
          countriesFilter={this.countriesFilter}
          regionsFilter={this.regionsFilter}
        />
      </div>
    );
  }
  price(value) {
    return value < 1 ? "" : value;
  }

  currency(value, negotiable) {
    return negotiable ? (
      <FormattedMessage id="home.ads.negotiable" defaultMessage="Negotiable" />
    ) : (
      value
    );
  }

  adsContent(ads) {
    let content = [];

    content.push(<Ads ads={ads.content} category="ads" />);
    content.push(
      ads.numOfElements > 0 ? (
        <Pagination
          key={"paginationList"}
          activePage={this.state.activePage}
          itemsCountPerPage={ads.pageSize}
          totalItemsCount={ads.numOfElements}
          pageRangeDisplayed={5}
          nextPageText="❯"
          prevPageText="❮"
          itemClassPrev="prev-next-arrows"
          itemClassNext="prev-next-arrows"
          onChange={this.handlePageChange.bind(this)}
        />
      ) : (
        <span>No ads</span>
      )
    );

    return content;
  }
}

const mapDispatchToProps = {
  fetchAds,
};

const mapStateToProps = (state) => ({
  state,
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
