import React from "react";
import "./../../alerts.css";
import { useDispatch } from "react-redux";
import { hideSignupSuccess } from "../../redux/actions";

export default ({ message }) => {
  const dispatch = useDispatch();

  return (
    <div className="modal-window">
      <div className="result success">
        <div className="result-img success">
          <img src="/result-success.png" />
        </div>
        <span className="result-message">{message}</span>
        <br />
        <button
          className="success"
          onClick={() => dispatch(hideSignupSuccess())}
        >
          Close
        </button>
      </div>
    </div>
  );
};
