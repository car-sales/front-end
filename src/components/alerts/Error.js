import React from "react";
import "./../../alerts.css";
import { useDispatch } from "react-redux";
import { hideSignupError } from "../../redux/actions";

export default ({ message }) => {
  const dispatch = useDispatch();

  return (
    <div className="modal-window">
      <div className="result error">
        <div className="result-img">
          <img src="/result-error.png" />
        </div>
        <span className="result-message">{message}</span>
        <br />
        <button className="error" onClick={() => dispatch(hideSignupError())}>
          Close
        </button>
      </div>
    </div>
  );
};
