import React from "react";
import { Link } from "react-router-dom";
import { FormattedMessage } from "react-intl";
import Popup from "reactjs-popup";
import { showDisabled } from "./../redux/actions";
import { useDispatch, useSelector } from "react-redux";

export default (props) => {
  var i = 1;
  console.log(props);
  var ad = props.ad;
  const dispatch = useDispatch();
  const disabled = useSelector((state) => {
    return state.app.disabled;
  });

  return (
    <div key={ad.id} className="ads">
      <div className="user-liked-action">
        <span
          className="action"
          onClick={() => {
            dispatch(showDisabled());
            props.unlikeAd(ad.id);
          }}
        >
          {disabled && <i class="fas fa-circle-notch fa-spin"></i>}
          &nbsp; Remove&nbsp;
        </span>
      </div>
      <div className="ad-image">
        <a href={"ad/" + ad.id}>
          <img
            src={`data:image/png;base64,${ad.image}`}
            width="100%"
            height="100%"
          />
        </a>
      </div>

      <a href={"ad/" + ad.id}>
        {ad.brand}&nbsp;{ad.model}
      </a>
      <br />
      {ad.price == -1 ? (
        <FormattedMessage
          id="home.ads.negotiable"
          defaultMessage="Negotiable"
        />
      ) : (
        ad.price + " " + ad.currency
      )}
    </div>
  );
};
