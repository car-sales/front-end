import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import { API_URL } from "../Constants";
import "./../loginStyle.css";
import { FormattedMessage } from "react-intl";
import $ from "jquery";
import { connect } from "react-redux";
import { signUp, showAlert } from "./../redux/actions";
import { Alert } from "./Alert";
import { Error } from "./alerts/Error";

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: "",
      password: "",
      lastName: "",
      firstName: "",
      phone: "",
      email: "",
      active: 0
    };
  }

  changeInputHandler = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  submitHandler = event => {
    event.preventDefault();

    if (!this.state.email.includes("@") || this.state.email.length < 4) {
      this.props.showAlert("Enter a valid email");
      return;
    }

    if (this.state.password.length < 6) {
      this.props.showAlert("Enter a password with at least 6 characters");
      return;
    }

    if (this.state.userName.length < 4) {
      this.props.showAlert("The user name must have at least 4 characters");
      return;
    }

    if (this.state.phone.length < 6) {
      this.props.showAlert("Enter a valid phone number");
      return;
    }

    this.props.signUp(this.state);
  };

  displayError(errorName) {
    $(".invalid-email").removeClass("error-active");
    $(".invalid-password").removeClass("error-active");
    $(".invalid-username").removeClass("error-active");
    $(".invalid-phone").removeClass("error-active");

    $(".invalid-" + errorName).addClass("error-active");
  }

  componentDidMount() {}

  render() {
    return (
      <div className="form-container">
        <div className="form">
          <form onSubmit={this.submitHandler}>
            <div className="user-logo-img">
              <img src="/login-user-image.png" width="100%" />
            </div>
            <div className="form-name">
              <span>
                <h2>
                  <FormattedMessage id="nav.signup" defaultMessage="Sign-up" />
                </h2>
              </span>
            </div>
            <div className="input-name">
              <FormattedMessage
                id="user.firstName"
                defaultMessage="First name"
              />
              :
            </div>
            <div>
              <input
                required
                type="text"
                name="firstName"
                value={this.state.firstName}
                onChange={this.changeInputHandler}
              />
            </div>
            <div className="input-name">
              <FormattedMessage id="user.lastName" defaultMessage="Last name" />
              :
            </div>
            <div>
              <input
                required
                type="text"
                name="lastName"
                value={this.state.lastName}
                onChange={this.changeInputHandler}
              />
            </div>

            <div className="input-name">
              <FormattedMessage id="user.email" defaultMessage="Email" />:
            </div>
            <div>
              <input
                required
                type="text"
                name="email"
                value={this.state.email}
                onChange={this.changeInputHandler}
              />
            </div>

            <div className="input-name">
              <FormattedMessage id="user.password" defaultMessage="Password" />:
            </div>
            <div>
              <input
                required
                type="password"
                name="password"
                value={this.state.password}
                onChange={this.changeInputHandler}
              />
            </div>
            <div className="input-name">
              <FormattedMessage id="user.userName" defaultMessage="Username" />:
            </div>
            <div>
              <input
                required
                type="text"
                name="userName"
                value={this.state.userName}
                onChange={this.changeInputHandler}
              />
            </div>
            <div className="input-name">
              <FormattedMessage id="user.phone" defaultMessage="Phone" />:
            </div>
            <div>
              <input
                required
                type="text"
                name="phone"
                value={this.state.phone}
                onChange={this.changeInputHandler}
              />
            </div>
            <div className="link">
              <Link to="/login">
                <FormattedMessage id="nav.login" defaultMessage="Login" />
              </Link>
            </div>
            <div>
              <button
                className="form-btn"
                type="submit"
                disabled={this.props.app.disabled}
              >
                {this.props.app.disabled && (
                  <i class="fas fa-circle-notch fa-spin"></i>
                )}
                &nbsp;
                <FormattedMessage id="contact.submit" defaultMessage="Submit" />
              </button>
            </div>
          </form>
        </div>
        <Alert text={this.props.app.alert} />
      </div>
    );
  }
}

const mapDispatchToProps = {
  signUp,
  showAlert
};

const mapStateToProps = state => state;

export default connect(mapStateToProps, mapDispatchToProps)(Register);
