import React, { Component } from "react";
import { Link } from "react-router-dom";
import { getCookie, logOut } from "../CookieManager";
import { FormattedMessage } from "react-intl";
import { switchLang } from "../Locale";
import $ from "jquery";

class Header extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  logoutHandler = (event) => {
    logOut();
    window.location.href = "/";
  };

  componentDidMount() {}

  displayLoader(lang) {
    // $(document).ready(function() {
    //   $(".loader").toggleClass("active");
    // });
    this.props.displayLoader();

    setTimeout(() => {
      switchLang(lang);
      // $(".loader").toggleClass("active");
      this.props.hideLoader();
      this.props.getLanguage();
    }, 700);
  }

  render() {
    return (
      <div className="header">
        <header>
          <div className="logo">
            <a className="header-link" href="/">
              <img src="/logo1+.png" width="110%" />
            </a>
          </div>
          <nav className="active">
            <ul>
              <li>
                <Link className="header-link" to="/">
                  <i className="fas fa-home"></i>&nbsp;
                  <FormattedMessage id="nav.home" defaultMessage="Home" />
                </Link>
              </li>

              <li>
                <Link className="header-link" to="/contact">
                  <i className="fas fa-envelope"></i>&nbsp;
                  <FormattedMessage id="nav.contact" defaultMessage="Contact" />
                </Link>
              </li>
              <li className="sub-menu">
                <Link className="header-link">
                  <i className="fas fa-user-circle"></i>&nbsp;
                  <FormattedMessage id="nav.account" defaultMessage="Account" />
                  &nbsp;
                  <i
                    className={
                      this.state.acc_caret
                        ? "fas fa-caret-up"
                        : "fas fa-caret-down"
                    }
                  />
                </Link>
                <ul>
                  <li>
                    {getCookie("access_token") == null ? (
                      <Link className="header-link" to="/login">
                        <i className="fas fa-user-alt"></i>&nbsp;
                        <FormattedMessage
                          id="nav.profile"
                          defaultMessage={`Profile`}
                        />
                      </Link>
                    ) : (
                      <Link to="/user">
                        <i className="fas fa-user"></i>&nbsp;
                        <FormattedMessage
                          id="nav.profile"
                          defaultMessage={`Profile`}
                        />
                      </Link>
                    )}
                    <div className="sub-menu-border"></div>
                  </li>
                  <li>
                    <Link className="header-link" to="/register">
                      <i className="fas fa-user-plus"></i>&nbsp;
                      <FormattedMessage
                        id="nav.signup"
                        defaultMessage={`Sign-up`}
                      />
                    </Link>
                    <div className="sub-menu-border"></div>
                  </li>
                  <hr className="split-line" />
                  <li>
                    {getCookie("access_token") == null ? (
                      <Link className="header-link" to="/login">
                        <i className="fas fa-sign-in-alt"></i>&nbsp;
                        <FormattedMessage
                          id="nav.login"
                          defaultMessage={`Login`}
                        />
                      </Link>
                    ) : (
                      <Link
                        className="header-link"
                        onClick={this.logoutHandler}
                      >
                        <i className="fas fa-sign-out-alt"></i>&nbsp;
                        <FormattedMessage
                          id="nav.logout"
                          defaultMessage={`Logout`}
                        />
                      </Link>
                    )}
                  </li>
                </ul>
              </li>
              <li className="sub-menu language">
                <Link>
                  <i className="fas fa-globe"></i>{" "}
                  <FormattedMessage
                    id="nav.language"
                    defaultMessage="Language"
                  />{" "}
                  <i
                    className={
                      this.state.lang_caret
                        ? "fas fa-caret-up"
                        : "fas fa-caret-down"
                    }
                  />
                </Link>
                <ul>
                  <li>
                    <Link
                      onClick={() => {
                        this.displayLoader("ro");
                      }}
                    >
                      <img src="/romania.svg" width="15px" />
                      <span>Română</span>
                    </Link>
                    <div className="sub-menu-border"></div>
                  </li>
                  <li>
                    <Link
                      onClick={() => {
                        this.displayLoader("en");
                      }}
                    >
                      <img src="/united-kingdom.svg" width="16px" />
                      <span>English</span>
                    </Link>
                    <div className="sub-menu-border"></div>
                  </li>
                  <li>
                    <Link
                      onClick={() => {
                        this.displayLoader("ru");
                      }}
                    >
                      <img src="/russia.svg" width="15px" />
                      <span>Русский</span>
                    </Link>
                  </li>
                </ul>
              </li>
            </ul>
          </nav>
          <div className="menu-toggle">
            <div className="line1"></div>
            <div className="line2"></div>
            <div className="line3"></div>
          </div>
        </header>
      </div>
    );
  }
}
export default Header;
