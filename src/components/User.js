import React, { Component } from "react";
import axios from "axios";
import { API_URL } from "../Constants";
import { setCookie, getCookie } from "../CookieManager";
import "./../userProfile.css";

class User extends Component {
  constructor(props) {
    super(props);

    this.state = {
      user: {}
    };
  }

  componentDidMount() {
    axios
      .get(`${API_URL}current-user`, {
        headers: {
          Authorization: getCookie("access_token")
        }
      })
      .then(Response => {
        const userData = Response.data;
        this.setState({
          user: userData
        });
      })
      .catch(error => {
        if (error.response) {
        }
      });
  }
  render() {
    return (
      <div>
        <span className="profile">Profile</span>
        <ul className="user-info">
          <li>
            User name:{" "}
            <span className="user-name">{this.state.user.userName}</span>
          </li>
          <li>
            First name: <span>{this.state.user.firstName}</span>
          </li>
          <li>
            Last name: <span>{this.state.user.lastName}</span>
          </li>
          <li>
            Email: <span>{this.state.user.email}</span>
          </li>
          <li>
            Phone: <span>{this.state.user.phone}</span>
          </li>
          <li>
            Created: <span>{this.state.user.createdAt}</span>
          </li>

          <li>
            <div>Ads:</div>
            {this.getAds(this.state.user.userAdDtos)}
          </li>

          <li>
            <div>Saved ads:</div>
            {this.getAds(this.state.user.likedAdDtos)}
          </li>
        </ul>
      </div>
    );
  }

  getAds(ads) {
    var adsList = [];
    if (ads != null) {
      ads.map(ad => {
        adsList.push(
          <div key={ad.id} className="ads">
            <a href={"ad/" + ad.id}>
              <div className="user-ad-image">
                <img
                  src={`data:image/png;base64,${ad.image}`}
                  width="100%"
                  height="100%"
                />
              </div>
            </a>
            <a href={"ad/" + ad.id}>
              {ad.brand}&nbsp;{ad.model}
            </a>
            <br />
            {ad.price}&nbsp;{ad.currency}
          </div>
        );
      });
    }
    return adsList;
  }
}

export default User;
