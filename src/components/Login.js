import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import "./../loginStyle.css";
import { API_URL } from "../Constants";
import { setCookie } from "../CookieManager";
import { FormattedMessage } from "react-intl";
import { connect } from "react-redux";
import { Alert } from "./Alert";
import { login, showAlert } from "./../redux/actions";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "ionciolacu15@gmail.com",
      password: "ciolacu.test",
    };
  }
  changeInputHandler = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  submitHandler = (event) => {
    event.preventDefault();
    this.props.login(this.state);
  };

  render() {
    return (
      <div className="form-container">
        <div className="form">
          <form onSubmit={this.submitHandler}>
            <div className="user-logo-img">
              <img src="/login-user-image.png" width="100%" />
            </div>
            <div className="form-name">
              <span>
                <h2>
                  <FormattedMessage id="nav.login" defaultMessage="Login" />
                </h2>
              </span>
            </div>
            <div className="input-name">
              <FormattedMessage id="user.email" defaultMessage="Email" />:
            </div>
            <div>
              <input
                required
                type="text"
                name="email"
                value={this.state.email}
                onChange={this.changeInputHandler}
              />
            </div>

            <div className="input-name">
              <FormattedMessage id="user.password" defaultMessage="Password" />:
            </div>
            <div>
              <input
                required
                type="password"
                name="password"
                value={this.state.password}
                onChange={this.changeInputHandler}
              />
            </div>
            <div className="link">
              <Link to="/register">
                <FormattedMessage id="nav.signup" defaultMessage="Sign-up" />
              </Link>
            </div>
            <div>
              <button
                className="form-btn"
                type="submit"
                disabled={this.props.app.disabled}
              >
                {this.props.app.disabled && (
                  <i class="fas fa-circle-notch fa-spin"></i>
                )}
                &nbsp;
                <FormattedMessage id="contact.submit" defaultMessage="Submit" />
              </button>
            </div>
          </form>
          <Alert text={this.props.app.alert} />
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = {
  login,
  showAlert,
};

const mapStateToProps = (state) => state;

export default connect(mapStateToProps, mapDispatchToProps)(Login);
