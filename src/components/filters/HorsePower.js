import React, { Component } from "react";
import filtersObj from "../../FiltersObj";
import { FormattedMessage } from "react-intl";
import { getRangeNames } from "../../Locale";

import "./../../filters.css";

class HorsePower extends Component {
  constructor(props) {
    super(props);

    this.state = {
      horsePowerStart: "",
      horsePowerEnd: ""
    };
  }

  componentDidMount() {
    this.setState({
      horsePowerStart:
        filtersObj.horsePowerStart > 10 ? filtersObj.horsePowerStart : "",
      horsePowerEnd:
        filtersObj.horsePowerEnd < 10000 ? filtersObj.horsePowerEnd : ""
    });
    this.props.horsePowerFilter(filtersObj);
  }

  horsePowerHandle = event => {
    filtersObj.isFiltered = true;

    var horsePowerObject = event.target;
    switch (horsePowerObject.name) {
      case "horsePowerStart": {
        if (horsePowerObject.valueAsNumber != horsePowerObject.valueAsNumber) {
          filtersObj[horsePowerObject.name] = 10;
        } else
          filtersObj[horsePowerObject.name] = horsePowerObject.valueAsNumber;

        break;
      }
      case "horsePowerEnd": {
        if (horsePowerObject.valueAsNumber != horsePowerObject.valueAsNumber) {
          filtersObj[horsePowerObject.name] = 10000;
        } else
          filtersObj[horsePowerObject.name] = horsePowerObject.valueAsNumber;

        break;
      }
    }

    this.props.horsePowerFilter(filtersObj);
  };

  horsePowerChangeHandler = e => {
    this.setState({
      [e.target.name]: e.target.valueAsNumber
    });
  };

  render() {
    return (
      <div className="filter">
        <div className="horsePower-block-title">
          <FormattedMessage
            id="home.filter.horsePower"
            defaultMessage="Horse power"
          />
        </div>
        <div className="filter-option">
          <FormattedMessage
            id="home.filter.from"
            defaultMessage={
              <input
                className="decimal-from-option"
                name="horsePowerStart"
                type="number"
                step="0.1"
                placeholder="from:"
                min="10"
                max="10000"
                onBlur={this.horsePowerHandle}
                value={this.state.horsePowerStart}
                onChange={this.horsePowerChangeHandler}
              />
            }
          >
            {value => (
              <input
                className="decimal-from-option"
                name="horsePowerStart"
                type="number"
                step="0.1"
                placeholder={`${value}:`}
                min="10"
                max="10000"
                onBlur={this.horsePowerHandle}
                value={this.state.horsePowerStart}
                onChange={this.horsePowerChangeHandler}
              />
            )}
          </FormattedMessage>
          <FormattedMessage
            id="home.filter.to"
            defaultMessage={
              <input
                className="decimal-to-option"
                name="horsePowerEnd"
                type="number"
                step="0.1"
                placeholder="to:"
                min="10"
                max="10000"
                onBlur={this.horsePowerHandle}
                value={this.state.horsePowerEnd}
                onChange={this.horsePowerChangeHandler}
              />
            }
          >
            {value => (
              <input
                className="decimal-to-option"
                name="horsePowerEnd"
                type="number"
                step="0.1"
                placeholder={`${value}:`}
                min="10"
                max="10000"
                onBlur={this.horsePowerHandle}
                value={this.state.horsePowerEnd}
                onChange={this.horsePowerChangeHandler}
              />
            )}
          </FormattedMessage>
        </div>
      </div>
    );
  }
}

export default HorsePower;
