import React, { Component } from "react";
import filtersObj from "../../FiltersObj";
import { FormattedMessage } from "react-intl";
import { getRangeNames } from "../../Locale";
import "./../../filters.css";

class Mileage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      mileageStart: "",
      mileageEnd: ""
    };
  }

  componentDidMount() {
    this.setState({
      mileageStart: filtersObj.mileageStart > 0 ? filtersObj.mileageStart : "",
      mileageEnd: filtersObj.mileageEnd < 1000000 ? filtersObj.mileageEnd : ""
    });
    this.props.mileageFilter(filtersObj);
  }

  mileageHandle = event => {
    filtersObj.isFiltered = true;

    var mileageObject = event.target;
    switch (mileageObject.name) {
      case "mileageStart": {
        if (mileageObject.valueAsNumber != mileageObject.valueAsNumber) {
          filtersObj[mileageObject.name] = 0;
        } else filtersObj[mileageObject.name] = mileageObject.valueAsNumber;

        break;
      }
      case "mileageEnd": {
        if (mileageObject.valueAsNumber != mileageObject.valueAsNumber) {
          filtersObj[mileageObject.name] = 1000000;
        } else filtersObj[mileageObject.name] = mileageObject.valueAsNumber;

        break;
      }
    }

    this.props.mileageFilter(filtersObj);
  };

  mileageChangeHandler = e => {
    this.setState({
      [e.target.name]: e.target.valueAsNumber
    });
  };

  render() {
    return (
      <div className="filter">
        <div className="mileage-block-title">
          <FormattedMessage id="home.filter.mileage" defaultMessage="Mileage" />
        </div>
        <div className="filter-option">
          <FormattedMessage
            id="home.filter.from"
            defaultMessage={
              <input
                className="decimal-from-option"
                name="mileageStart"
                type="number"
                placeholder="from:"
                min="0"
                max="1000000"
                onBlur={this.mileageHandle}
                value={this.state.mileageStart}
                onChange={this.mileageChangeHandler}
              />
            }
          >
            {value => (
              <input
                className="decimal-from-option"
                name="mileageStart"
                type="number"
                placeholder={`${value}:`}
                min="0"
                max="1000000"
                onBlur={this.mileageHandle}
                value={this.state.mileageStart}
                onChange={this.mileageChangeHandler}
              />
            )}
          </FormattedMessage>
          <FormattedMessage
            id="home.filter.to"
            defaultMessage={
              <input
                className="decimal-to-option"
                name="mileageEnd"
                type="number"
                placeholder="to:"
                min="0"
                max="1000000"
                onBlur={this.mileageHandle}
                value={this.state.mileageEnd}
                onChange={this.mileageChangeHandler}
              />
            }
          >
            {value => (
              <input
                className="decimal-to-option"
                name="mileageEnd"
                type="number"
                placeholder={`${value}:`}
                min="0"
                max="1000000"
                onBlur={this.mileageHandle}
                value={this.state.mileageEnd}
                onChange={this.mileageChangeHandler}
              />
            )}
          </FormattedMessage>
        </div>
      </div>
    );
  }
}

export default Mileage;
