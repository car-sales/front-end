import React, { Component } from "react";
import filtersObj from "../../FiltersObj";
import { get } from "../../GetRequest";
import { setData, filterHandle } from "../../Util";
import { FormattedMessage } from "react-intl";
import $ from "jquery";
import "./../../filters.css";

class BodyStyle extends Component {
  constructor(props) {
    super(props);

    this.state = {
      bodyStyles: [],
      cargo_van: false,
      convertible: false,
      coupe: false,
      crew_cab_pickup: false,
      extended_cab_pickup: false,
      hatchback: false,
      minivan: false,
      passenger_van: false,
      regular_cab_pickup: false,
      suv: false,
      sedan: false,
      wagon: false,
    };
  }

  componentDidMount() {
    if (filtersObj.bodyStyles.length > 0) {
      $(".bodyStyles-block-title")
        .addClass("bodyStyles-block-active")
        .next()
        .slideToggle(150);
    }
    $(document).ready(function () {
      $(".bodyStyles-block-title").click(function (event) {
        $(this).toggleClass("bodyStyles-block-active").next().slideToggle(150);
      });
    });

    get("bodyStyles")
      .then((Response) => {
        this.setState(
          {
            bodyStyles: Response.data,
          },
          function () {
            setData("bodyStyles", this.setStateToTrue);
          }
        );
      })
      .catch((error) => {
        if (error.response) {
        }
      });
    this.props.bodyStylesFilter(filtersObj);
  }

  setStateToTrue = (name) => {
    this.setState({
      [name]: true,
    });
  };

  bodyStyleHandle(bodyStyle) {
    this.setState({
      updated: true,
    });

    this.setState(
      {
        [bodyStyle]: !this.state[bodyStyle],
      },
      function () {
        filtersObj.isFiltered = true;
        filterHandle("bodyStyles", bodyStyle, this.state[bodyStyle]);
        this.props.bodyStylesFilter(filtersObj);
      }
    );
  }

  render() {
    return (
      <div className="filter">
        <div className="bodyStyles-block-title">
          <FormattedMessage
            id="home.filter.bodyStyles"
            defaultMessage="Body styles"
          />
        </div>
        <div className="block-container">
          {this.state.bodyStyles.length > 0
            ? this.state.bodyStyles.map((bodyStyle) => (
                <div key={bodyStyle} className="filter-option">
                  <input
                    id={bodyStyle}
                    type="checkbox"
                    onChange={() =>
                      this.bodyStyleHandle(this.bodyStyleToLowerCase(bodyStyle))
                    }
                    checked={this.state[this.bodyStyleToLowerCase(bodyStyle)]}
                  />
                  <label for={bodyStyle} className="filter-option-name">
                    <FormattedMessage
                      id={
                        "home.filter.bodyStyles." +
                        this.bodyStyleToLowerCase(bodyStyle)
                      }
                      defaultMessage={bodyStyle}
                    />
                  </label>
                </div>
              ))
            : ""}
        </div>
      </div>
    );
  }

  bodyStyleToLowerCase(bodyStyle) {
    let b = bodyStyle;
    b = b.replace(" ", "_");

    return b.toLowerCase();
  }
}

export default BodyStyle;
