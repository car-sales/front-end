import React, { Component } from "react";
import filtersObj from "../../FiltersObj";
import { get } from "../../GetRequest";
import { setData, filterHandle } from "../../Util";
import { FormattedMessage } from "react-intl";
import $ from "jquery";
import "./../../filters.css";
import { toLowerCaseV2 } from "../../Util";

class InteriorColor extends Component {
  constructor(props) {
    super(props);

    this.state = {
      interiorColors: [],
    };
  }

  componentDidMount() {
    if (filtersObj.interiorColors.length > 0) {
      $(".interiorColors-block-title")
        .addClass("interiorColors-block-active")
        .next()
        .slideToggle(150);
    }
    $(document).ready(function () {
      $(".interiorColors-block-title").click(function (event) {
        $(this)
          .toggleClass("interiorColors-block-active")
          .next()
          .slideToggle(150);
      });
    });

    get("colors")
      .then((Response) => {
        this.setState(
          {
            interiorColors: Response.data,
          },
          function () {
            setData("interiorColors", this.setStateToTrue);
          }
        );
      })
      .catch((error) => {
        if (error.response) {
        }
      });
    this.props.interiorColorsFilter(filtersObj);
  }

  setStateToTrue = (name) => {
    this.setState({
      [name]: true,
    });
  };

  interiorColorHandle(interiorColor) {
    filtersObj.isFiltered = true;

    this.setState({
      updated: true,
    });

    this.setState(
      {
        [interiorColor]: !this.state[interiorColor],
      },
      function () {
        filterHandle(
          "interiorColors",
          interiorColor,
          this.state[interiorColor]
        );
        this.props.interiorColorsFilter(filtersObj);
      }
    );
  }

  render() {
    return (
      <div className="filter">
        <div className="interiorColors-block-title">
          <FormattedMessage
            id="home.filter.interiorColors"
            defaultMessage="Interior colors"
          />
        </div>
        <div className="block-container">
          {this.state.interiorColors.length > 0
            ? this.state.interiorColors.map((interiorColor) => (
                <div key={interiorColor} className="filter-option color-filter">
                  <div
                    className="color-div"
                    style={{
                      backgroundColor:
                        interiorColor == "Brown"
                          ? "saddlebrown"
                          : interiorColor,
                    }}
                  ></div>

                  <label for={interiorColor} className="filter-option-name">
                    <FormattedMessage
                      id={"home.filter.colors." + toLowerCaseV2(interiorColor)}
                      defaultMessage={interiorColor}
                    />
                  </label>
                  <input
                    id={interiorColor}
                    type="checkbox"
                    onChange={() =>
                      this.interiorColorHandle(this.toLowerCase(interiorColor))
                    }
                    checked={this.state[this.toLowerCase(interiorColor)]}
                  />
                </div>
              ))
            : ""}
        </div>
      </div>
    );
  }

  toLowerCase(interiorColor) {
    let t = interiorColor;

    t = t.replace(" ", "_");

    return t.toLowerCase();
  }
}

export default InteriorColor;
