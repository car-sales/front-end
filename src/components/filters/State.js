import React, { Component } from "react";
import filtersObj from "../../FiltersObj";
import { filterHandle } from "../../Util";
import { FormattedMessage } from "react-intl";
import $ from "jquery";
import "./../../filters.css";
import { toLowerCaseV2 } from "../../Util";

class State extends Component {
  constructor(props) {
    super(props);

    this.state = {
      states: ["New", "Used", "Needs Repair"]
    };
  }

  componentDidMount() {
    if (filtersObj.states.length > 0) {
      $(".states-block-title")
        .addClass("states-block-active")
        .next()
        .slideToggle(150);
    }
    $(document).ready(function() {
      $(".states-block-title").click(function(event) {
        $(this)
          .toggleClass("states-block-active")
          .next()
          .slideToggle(150);
      });
    });

    if (filtersObj.states.length > 0)
      for (let i = 0; i < this.state.states.length; i++) {
        if (
          filtersObj.states.indexOf(this.replaceSpaces(this.state.states[i])) !=
          -1
        ) {
          this.setStateToTrue(this.replaceSpaces(this.state.states[i]));
        }
      }

    this.props.statesFilter(filtersObj);
  }

  setStateToTrue = name => {
    this.setState({
      [name]: true
    });
  };

  steerWheelSideHandle(state) {
    filtersObj.isFiltered = true;

    this.setState({
      updated: true
    });

    this.setState(
      {
        [state]: !this.state[state]
      },
      function() {
        filterHandle("states", state, this.state[state]);
        this.props.statesFilter(filtersObj);
      }
    );
  }

  render() {
    return (
      <div className="filter">
        <div className="states-block-title">
          {" "}
          <FormattedMessage id="home.filter.state" defaultMessage="State" />
        </div>
        <div className="block-container">
          {this.state.states.length > 0
            ? this.state.states.map(state => (
                <div key={state} className="filter-option">
                  <input
                    id={state}
                    type="checkbox"
                    onChange={() =>
                      this.steerWheelSideHandle(this.replaceSpaces(state))
                    }
                    checked={this.state[this.replaceSpaces(state)]}
                  />
                  <label for={state} className="filter-option-name">
                    <FormattedMessage
                      id={"home.filter.state." + toLowerCaseV2(state)}
                      defaultMessage={state}
                    />
                  </label>
                </div>
              ))
            : ""}
        </div>
      </div>
    );
  }

  replaceSpaces(side) {
    return side.replace(" ", "_");
  }
}

export default State;
