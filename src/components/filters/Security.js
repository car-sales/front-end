import React, { Component } from "react";
import filtersObj from "../../FiltersObj";
import { get } from "../../GetRequest";
import { setData, filterHandle } from "../../Util";
import { FormattedMessage } from "react-intl";
import $ from "jquery";
import "./../../filters.css";
import { toLowerCaseV2 } from "../../Util";

class Security extends Component {
  constructor(props) {
    super(props);

    this.state = {
      securities: [],
    };
  }

  componentDidMount() {
    if (filtersObj.securities.length > 0) {
      $(".securities-block-title")
        .addClass("securities-block-active")
        .next()
        .slideToggle(150);
    }
    $(document).ready(function () {
      $(".securities-block-title").click(function (event) {
        $(this).toggleClass("securities-block-active").next().slideToggle(150);
      });
    });

    get("securities")
      .then((Response) => {
        this.setState(
          {
            securities: Response.data,
          },
          function () {
            var securities = [];
            Response.data.map((security) => {
              securities.push(this.replaceSpaces(security.securityName));
            });
            setData("securities", this.setStateToTrue);
          }
        );
      })
      .catch((error) => {
        if (error.response) {
        }
      });
    this.props.securitiesFilter(filtersObj);
  }

  setStateToTrue = (name) => {
    this.setState({
      [name]: true,
    });
  };

  securityHandle(security) {
    filtersObj.isFiltered = true;

    this.setState({
      updated: true,
    });

    this.setState(
      {
        [security]: !this.state[security],
      },
      function () {
        filterHandle("securities", security, this.state[security]);
        this.props.securitiesFilter(filtersObj);
      }
    );
  }

  render() {
    return (
      <div className="filter">
        <div className="securities-block-title">
          <FormattedMessage
            id="home.filter.securities"
            defaultMessage="Securities"
          />
        </div>
        <div className="block-container">
          {this.state.securities.length > 0
            ? this.state.securities.map((security) => (
                <div
                  key={this.replaceSpaces(security.securityName)}
                  className="filter-option"
                >
                  <input
                    id={security.securityName}
                    type="checkbox"
                    onChange={() =>
                      this.securityHandle(
                        this.replaceSpaces(security.securityName)
                      )
                    }
                    checked={
                      this.state[this.replaceSpaces(security.securityName)]
                    }
                  />
                  <label
                    for={security.securityName}
                    className="filter-option-name"
                  >
                    <FormattedMessage
                      id={
                        "home.filter.securities." +
                        toLowerCaseV2(security.securityName)
                      }
                      defaultMessage={security.securityName}
                    />
                  </label>
                </div>
              ))
            : ""}
        </div>
      </div>
    );
  }

  replaceSpaces(security) {
    let t = security;
    t = t.replace(" ", "_");

    return t;
  }
}

export default Security;
