import React, { Component } from "react";
import filtersObj from "../../FiltersObj";
import { filterHandle } from "../../Util";
import { FormattedMessage } from "react-intl";
import $ from "jquery";
import "./../../filters.css";
import { toLowerCaseV2 } from "../../Util";

class SteerWheelSide extends Component {
  constructor(props) {
    super(props);

    this.state = {
      steerWheelSides: ["Left", "Right"]
    };
  }

  componentDidMount() {
    if (filtersObj.steerWheelSides.length > 0) {
      $(".steerWheelSides-block-title")
        .addClass("steerWheelSides-block-active")
        .next()
        .slideToggle(150);
    }
    $(document).ready(function() {
      $(".steerWheelSides-block-title").click(function(event) {
        $(this)
          .toggleClass("steerWheelSides-block-active")
          .next()
          .slideToggle(150);
      });
    });

    if (filtersObj.steerWheelSides.length > 0)
      for (let i = 0; i < this.state.steerWheelSides.length; i++) {
        if (
          filtersObj.steerWheelSides.indexOf(this.state.steerWheelSides[i]) !=
          -1
        ) {
          this.setStateToTrue(this.state.steerWheelSides[i]);
        }
      }

    this.props.steerWheelSidesFilter(filtersObj);
  }

  setStateToTrue = name => {
    this.setState({
      [name]: true
    });
  };

  steerWheelSideHandle(side) {
    filtersObj.isFiltered = true;

    this.setState({
      updated: true
    });

    this.setState(
      {
        [side]: !this.state[side]
      },
      function() {
        filterHandle("steerWheelSides", side, this.state[side]);
        this.props.steerWheelSidesFilter(filtersObj);
      }
    );
  }

  render() {
    return (
      <div className="filter">
        <div className="steerWheelSides-block-title">
          <FormattedMessage
            id="home.filter.steerWheel"
            defaultMessage="Steer wheel"
          />
        </div>
        <div className="block-container">
          {this.state.steerWheelSides.length > 0
            ? this.state.steerWheelSides.map(side => (
                <div key={side} className="filter-option">
                  <input
                    id={side}
                    type="checkbox"
                    onChange={() => this.steerWheelSideHandle(side)}
                    checked={this.state[side]}
                  />
                  <label for={side} className="filter-option-name">
                    <FormattedMessage
                      id={"home.filter.steerWheels." + toLowerCaseV2(side)}
                      defaultMessage={side}
                    />
                  </label>
                </div>
              ))
            : ""}
        </div>
      </div>
    );
  }
}

export default SteerWheelSide;
