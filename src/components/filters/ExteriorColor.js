import React, { Component } from "react";
import filtersObj from "../../FiltersObj";
import { get } from "../../GetRequest";
import { setData, filterHandle } from "../../Util";
import { FormattedMessage } from "react-intl";
import $ from "jquery";
import "./../../filters.css";
import { toLowerCaseV2 } from "../../Util";

class ExteriorColor extends Component {
  constructor(props) {
    super(props);

    this.state = {
      exteriorColors: [],
    };
  }

  componentDidMount() {
    if (filtersObj.exteriorColors.length > 0) {
      $(".exteriorColors-block-title")
        .addClass("exteriorColors-block-active")
        .next()
        .slideToggle(150);
    }
    $(document).ready(function () {
      $(".exteriorColors-block-title").click(function (event) {
        $(this)
          .toggleClass("exteriorColors-block-active")
          .next()
          .slideToggle(150);
      });
    });

    get("colors")
      .then((Response) => {
        this.setState(
          {
            exteriorColors: Response.data,
          },
          function () {
            setData("exteriorColors", this.setStateToTrue);
          }
        );
      })
      .catch((error) => {
        if (error.response) {
        }
      });
    this.props.exteriorColorsFilter(filtersObj);
  }

  setStateToTrue = (name) => {
    this.setState({
      [name]: true,
    });
  };

  exteriorColorHandle(exteriorColor) {
    filtersObj.isFiltered = true;

    this.setState({
      updated: true,
    });

    this.setState(
      {
        [exteriorColor]: !this.state[exteriorColor],
      },
      function () {
        filterHandle(
          "exteriorColors",
          exteriorColor,
          this.state[exteriorColor]
        );
        this.props.exteriorColorsFilter(filtersObj);
      }
    );
  }

  render() {
    return (
      <div className="filter">
        <div className="exteriorColors-block-title">
          <FormattedMessage
            id="home.filter.exteriorColors"
            defaultMessage="Exterior colors"
          />
        </div>
        <div className="block-container">
          {this.state.exteriorColors.length > 0
            ? this.state.exteriorColors.map((exteriorColor) => (
                <div key={exteriorColor} className="filter-option color-filter">
                  <div
                    className="color-div"
                    style={{
                      backgroundColor:
                        exteriorColor == "Brown"
                          ? "saddlebrown"
                          : exteriorColor,
                    }}
                  ></div>

                  <label for={exteriorColor} className="filter-option-name">
                    <FormattedMessage
                      id={"home.filter.colors." + toLowerCaseV2(exteriorColor)}
                      defaultMessage={this.toLowerCase(exteriorColor)}
                    />
                  </label>
                  <input
                    id={exteriorColor}
                    type="checkbox"
                    onChange={() =>
                      this.exteriorColorHandle(this.toLowerCase(exteriorColor))
                    }
                    checked={this.state[this.toLowerCase(exteriorColor)]}
                  />
                </div>
              ))
            : ""}
        </div>
      </div>
    );
  }

  toLowerCase(exteriorColor) {
    let t = exteriorColor;

    t = t.replace(" ", "_");

    return t.toLowerCase();
  }
}

export default ExteriorColor;
