import React, { Component } from "react";
import filtersObj from "../../FiltersObj";
import { get } from "../../GetRequest";
import { setData, filterHandle } from "../../Util";
import { FormattedMessage } from "react-intl";
import $ from "jquery";
import "./../../filters.css";
import { toLowerCaseV2 } from "../../Util";

class Traction extends Component {
  constructor(props) {
    super(props);

    this.state = {
      tractions: [],
    };
  }

  componentDidMount() {
    if (filtersObj.tractions.length > 0) {
      $(".tractions-block-title")
        .addClass("tractions-block-active")
        .next()
        .slideToggle(150);
    }
    $(document).ready(function () {
      $(".tractions-block-title").click(function (event) {
        $(this).toggleClass("tractions-block-active").next().slideToggle(150);
      });
    });

    get("traction")
      .then((Response) => {
        this.setState(
          {
            tractions: Response.data,
          },
          function () {
            setData("tractions", this.setStateToTrue);
          }
        );
      })
      .catch((error) => {
        if (error.response) {
        }
      });
    this.props.tractionsFilter(filtersObj);
  }

  setStateToTrue = (name) => {
    this.setState({
      [name]: true,
    });
  };

  tractionHandle(traction) {
    filtersObj.isFiltered = true;

    this.setState({
      updated: true,
    });

    this.setState(
      {
        [traction]: !this.state[traction],
      },
      function () {
        filterHandle("tractions", traction, this.state[traction]);
        this.props.tractionsFilter(filtersObj);
      }
    );
  }

  render() {
    return (
      <div className="filter">
        <div className="tractions-block-title">
          <FormattedMessage
            id="home.filter.tractions"
            defaultMessage="Tractions"
          />
        </div>
        <div className="block-container">
          {this.state.tractions.length > 0
            ? this.state.tractions.map((traction) => (
                <div key={traction} className="filter-option">
                  <input
                    id={traction}
                    type="checkbox"
                    onChange={() =>
                      this.tractionHandle(this.toLowerCase(traction))
                    }
                    checked={this.state[this.toLowerCase(traction)]}
                  />
                  <label for={traction} className="filter-option-name">
                    <FormattedMessage
                      id={"home.filter.tractions." + toLowerCaseV2(traction)}
                      defaultMessage={traction}
                    />
                  </label>
                </div>
              ))
            : ""}
        </div>
      </div>
    );
  }

  toLowerCase(traction) {
    let t = traction;

    t = t.replace(" ", "_");

    return t.toLowerCase();
  }
}

export default Traction;
