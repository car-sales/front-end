import React, { Component } from "react";
import filtersObj from "../../FiltersObj";
import { get } from "../../GetRequest";
import { setData, filterHandle } from "../../Util";
import $ from "jquery";
import "./../../filters.css";

class Region extends Component {
  constructor(props) {
    super(props);

    this.state = {
      countries: [],
      regions: []
    };
  }

  componentDidMount() {
    if (this.props.country.countryName == "Moldova") {
      $(document).ready(function() {
        $(".regions-block-title").click(function(event) {
          $(this)
            .toggleClass("regions-block-active")
            .next()
            .slideToggle(150);
        });
      });
    }
  }

  setStateToTrue = name => {
    this.setState({
      [name]: true
    });
  };

  countryHandle = countryName => {
    filtersObj.isFiltered = true;

    this.props.countryHandle(countryName);
  };

  regionHandle = regionName => {
    filtersObj.isFiltered = true;

    this.props.regionHandle(regionName, this.props.country.countryName);
  };

  render() {
    return (
      <div className="regions">
        <div className="regions-block-title">
          <input
            type="checkbox"
            onChange={() => this.countryHandle(this.props.country.countryName)}
            checked={this.props.countryChecked[this.props.country.countryName]}
          />
          {this.props.country.countryName}
        </div>

        <div className="block-container regions-block-container">
          {this.props.country.regions.length > 0
            ? this.props.country.regions.map(region => (
                <div key={region.regionName} className="filter">
                  <input
                    id={region.regionName}
                    type="checkbox"
                    onChange={() => this.regionHandle(region.regionName)}
                    checked={this.props.countryChecked[region.regionName]}
                  />
                  <label for={region.regionName} className="filter-option-name">
                    {region.regionName}
                  </label>
                </div>
              ))
            : ""}
        </div>
      </div>
    );
  }
}

export default Region;
