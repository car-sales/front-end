import React, { Component } from "react";
import filtersObj from "../../FiltersObj";
import { get } from "../../GetRequest";
import { setData, filterHandle } from "../../Util";
import $ from "jquery";
import "./../../filters.css";
import { FormattedMessage } from "react-intl";
import { toLowerCaseV2 } from "../../Util";

class Comfort extends Component {
  constructor(props) {
    super(props);

    this.state = {
      comforts: [],
    };
  }

  componentDidMount() {
    if (filtersObj.comforts.length > 0) {
      $(".comforts-block-title")
        .addClass("comforts-block-active")
        .next()
        .slideToggle(150);
    }
    $(document).ready(function () {
      $(".comforts-block-title").click(function (event) {
        $(this).toggleClass("comforts-block-active").next().slideToggle(150);
      });
    });

    get("comforts")
      .then((Response) => {
        this.setState(
          {
            comforts: Response.data,
          },
          function () {
            var comforts = [];
            Response.data.map((comfort) => {
              comforts.push(comfort.comfortName);
            });
            setData("comforts", this.setStateToTrue);
          }
        );
      })
      .catch((error) => {
        if (error.response) {
        }
      });
    this.props.comfortsFilter(filtersObj);
  }

  setStateToTrue = (name) => {
    this.setState({
      [name]: true,
    });
  };

  comfortHandle(comfort) {
    filtersObj.isFiltered = true;

    this.setState({
      updated: true,
    });

    this.setState(
      {
        [comfort]: !this.state[comfort],
      },
      function () {
        filterHandle("comforts", comfort, this.state[comfort]);
        this.props.comfortsFilter(filtersObj);
      }
    );
  }

  render() {
    return (
      <div className="filter">
        <div className="comforts-block-title">
          <FormattedMessage
            id="home.filter.comforts"
            defaultMessage="Comforts"
          />
        </div>
        <div className="block-container">
          {this.state.comforts.length > 0
            ? this.state.comforts.map((comfort) => (
                <div
                  key={this.replaceSpaces(comfort.comfortName)}
                  className="filter-option"
                >
                  <input
                    id={comfort.comfortName}
                    type="checkbox"
                    onChange={() =>
                      this.comfortHandle(
                        this.replaceSpaces(comfort.comfortName)
                      )
                    }
                    checked={
                      this.state[this.replaceSpaces(comfort.comfortName)]
                    }
                  />
                  <label
                    for={comfort.comfortName}
                    className="filter-option-name"
                  >
                    <FormattedMessage
                      id={
                        "home.filter.comforts." +
                        toLowerCaseV2(comfort.comfortName)
                      }
                      defaultMessage={comfort.comfortName}
                    />
                  </label>
                </div>
              ))
            : ""}
        </div>
      </div>
    );
  }

  replaceSpaces(comfort) {
    let t = comfort;
    t = t.replace(" ", "_");

    return t;
  }
}

export default Comfort;
