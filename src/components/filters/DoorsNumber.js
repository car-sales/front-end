import React, { Component } from "react";
import filtersObj from "../../FiltersObj";
import $ from "jquery";
import "./../../filters.css";
import { FormattedMessage } from "react-intl";

class DoorsNumber extends Component {
  constructor(props) {
    super(props);

    this.state = {
      doorsNumbers: [2, 3, 4, 5]
    };
  }

  componentDidMount() {
    if (filtersObj.doorsNumbers.length > 0) {
      $(".doorsNumbers-block-title")
        .addClass("doorsNumbers-block-active")
        .next()
        .slideToggle(150);
    }
    $(document).ready(function() {
      $(".doorsNumbers-block-title").click(function(event) {
        $(this)
          .toggleClass("doorsNumbers-block-active")
          .next()
          .slideToggle(150);
      });
    });

    if (filtersObj.doorsNumbers.length > 0)
      for (let i = 0; i < this.state.doorsNumbers.length; i++) {
        if (filtersObj.doorsNumbers.indexOf(this.state.doorsNumbers[i]) != -1) {
          this.setStateToTrue(
            this.getNumberToString(this.state.doorsNumbers[i])
          );
        }
      }

    this.props.doorsNumbersFilter(filtersObj);
  }

  setStateToTrue = name => {
    this.setState({
      [name]: true
    });
  };

  doorsNumberHandle(doorsNumber) {
    filtersObj.isFiltered = true;

    this.setState({
      updated: true
    });

    this.setState(
      {
        [this.getNumberToString(doorsNumber)]: !this.state[
          this.getNumberToString(doorsNumber)
        ]
      },
      function() {
        let dataList = filtersObj.doorsNumbers;
        if (this.state[this.getNumberToString(doorsNumber)]) {
          dataList.push(doorsNumber);
          filtersObj.doorsNumbers = dataList;
        } else {
          var idx = dataList.indexOf(doorsNumber);
          if (idx !== -1) dataList.splice(idx, 1);

          filtersObj.doorsNumbers = dataList;
        }
        this.props.doorsNumbersFilter(filtersObj);
      }
    );
  }

  render() {
    return (
      <div className="filter">
        <div className="doorsNumbers-block-title">
          <FormattedMessage
            id="home.filter.doorsNumber"
            defaultMessage="Doors number"
          />
        </div>
        <div className="block-container">
          {this.state.doorsNumbers.length > 0
            ? this.state.doorsNumbers.map(doorsNumber => (
                <div key={doorsNumber} className="filter-option">
                  <input
                    id={"d" + doorsNumber}
                    type="checkbox"
                    onChange={() => this.doorsNumberHandle(doorsNumber)}
                    checked={this.state[this.getNumberToString(doorsNumber)]}
                  />
                  <label for={"d" + doorsNumber} className="filter-option-name">
                    {doorsNumber}
                  </label>
                </div>
              ))
            : ""}
        </div>
      </div>
    );
  }

  getNumberToString(number) {
    switch (number) {
      case 2:
        return "two";
      case 3:
        return "three";
      case 4:
        return "four";
      case 5:
        return "five";
    }
  }

  getStringToNumber(number) {
    switch (number) {
      case "two":
        return 2;
      case "three":
        return 3;
      case "four":
        return 4;
      case "five":
        return 5;
    }
  }
}

export default DoorsNumber;
