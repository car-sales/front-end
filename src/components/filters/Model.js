import React, { Component } from "react";
import $ from "jquery";
import "./../../filters.css";

class Models extends Component {
  constructor(props) {
    super(props);

    this.state = {
      brands: [],
      models: []
    };
  }

  componentDidMount() {
    if (this.props.brand.id === 545) {
      $(document).ready(function() {
        $(".models-block-title").click(function(event) {
          $(this)
            .toggleClass("models-block-active")
            .next()
            .slideToggle(150);
        });
      });
    }
  }

  setStateToTrue = name => {
    this.setState({
      [name]: true
    });
  };

  brandHandle = brandName => {
    this.props.brandHandle(brandName);
  };

  modelHandle = modelName => {
    this.props.modelHandle(modelName, this.props.brand.brandName);
  };

  render() {
    return (
      <div className="models">
        <div className="models-block-title">
          <input
            type="checkbox"
            onChange={() => this.brandHandle(this.props.brand.brandName)}
            checked={this.props.brandChecked[this.props.brand.brandName]}
          />
          {this.props.brand.brandName}
        </div>

        <div className="block-container models-block-container">
          {this.props.brand.models.length > 0
            ? this.props.brand.models.map(model => (
                <div key={model.id} className="filter">
                  <input
                    id={model.modelName}
                    type="checkbox"
                    onChange={() => this.modelHandle(model.modelName)}
                    checked={this.props.brandChecked[model.modelName]}
                  />
                  <label for={model.modelName} className="filter-option-name">
                    {model.modelName}
                  </label>
                </div>
              ))
            : ""}
        </div>
      </div>
    );
  }

  toLowerCase(model) {
    let m = model;
    m = m.replace("-", "_");
    m = m.replace(" ", "_");

    return m.toLowerCase();
  }
}

export default Models;
