import React, { Component } from "react";
import filtersObj from "../../FiltersObj";
import { FormattedMessage } from "react-intl";
import { getRangeNames } from "../../Locale";
import "./../../filters.css";

class Capacity extends Component {
  constructor(props) {
    super(props);

    this.state = {
      capacityStart: "",
      capacityEnd: ""
    };
  }

  componentDidMount() {
    this.setState({
      capacityStart:
        filtersObj.capacityStart > 0.5 ? filtersObj.capacityStart : "",
      capacityEnd: filtersObj.capacityEnd < 10 ? filtersObj.capacityEnd : ""
    });
    this.props.capacityFilter(filtersObj);
  }

  capacityHandle = event => {
    filtersObj.isFiltered = true;

    var capacityObject = event.target;
    switch (capacityObject.name) {
      case "capacityStart": {
        if (capacityObject.valueAsNumber != capacityObject.valueAsNumber) {
          filtersObj[capacityObject.name] = 0.5;
        } else filtersObj[capacityObject.name] = capacityObject.valueAsNumber;

        break;
      }
      case "capacityEnd": {
        if (capacityObject.valueAsNumber != capacityObject.valueAsNumber) {
          filtersObj[capacityObject.name] = 10;
        } else filtersObj[capacityObject.name] = capacityObject.valueAsNumber;

        break;
      }
    }

    this.props.capacityFilter(filtersObj);
  };

  capacityChangeHandler = e => {
    this.setState({
      [e.target.name]: e.target.valueAsNumber
    });
  };

  render() {
    return (
      <div className="filter">
        <div className="capacity-block-title">
          <FormattedMessage
            id="home.filter.capacity"
            defaultMessage="Capacity"
          />{" "}
          <FormattedMessage id="home.filter.capacity.in" defaultMessage="in" />{" "}
          <FormattedMessage
            id="home.filter.capacity.liters"
            defaultMessage="liters"
          />
        </div>
        <div className="filter-option">
          <FormattedMessage
            id="home.filter.from"
            defaultMessage={
              <input
                className="decimal-from-option"
                name="capacityStart"
                type="number"
                step="0.1"
                placeholder="from:"
                min="0.5"
                max="10"
                onBlur={this.capacityHandle}
                value={this.state.capacityStart}
                onChange={this.capacityChangeHandler}
              />
            }
          >
            {value => (
              <input
                className="decimal-from-option"
                name="capacityStart"
                type="number"
                step="0.1"
                placeholder={`${value}:`}
                min="0.5"
                max="10"
                onBlur={this.capacityHandle}
                value={this.state.capacityStart}
                onChange={this.capacityChangeHandler}
              />
            )}
          </FormattedMessage>
          <FormattedMessage
            id="home.filter.to"
            defaultMessage={
              <input
                className="decimal-to-option"
                name="capacityEnd"
                type="number"
                step="0.1"
                placeholder="to:"
                min="0.5"
                max="10"
                onBlur={this.capacityHandle}
                value={this.state.capacityEnd}
                onChange={this.capacityChangeHandler}
              />
            }
          >
            {value => (
              <input
                className="decimal-to-option"
                name="capacityEnd"
                type="number"
                step="0.1"
                placeholder={`${value}:`}
                min="0.5"
                max="10"
                onBlur={this.capacityHandle}
                value={this.state.capacityEnd}
                onChange={this.capacityChangeHandler}
              />
            )}
          </FormattedMessage>
        </div>
      </div>
    );
  }
}

export default Capacity;
