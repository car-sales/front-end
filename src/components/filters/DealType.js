import React, { Component } from "react";
import filtersObj from "../../FiltersObj";
import { filterHandle } from "../../Util";
import $ from "jquery";
import "./../../filters.css";
import { FormattedMessage } from "react-intl";
import { toLowerCaseV2 } from "../../Util";

class DealType extends Component {
  constructor(props) {
    super(props);

    this.state = {
      dealTypes: ["Sale", "Buy", "Change"]
    };
  }

  componentDidMount() {
    if (filtersObj.dealTypes.length > 0) {
      $(".dealTypes-block-title")
        .addClass("dealTypes-block-active")
        .next()
        .slideToggle(150);
    }
    $(document).ready(function() {
      $(".dealTypes-block-title").click(function(event) {
        $(this)
          .toggleClass("dealTypes-block-active")
          .next()
          .slideToggle(150);
      });
    });

    if (filtersObj.dealTypes.length > 0)
      for (let i = 0; i < this.state.dealTypes.length; i++) {
        if (filtersObj.dealTypes.indexOf(this.state.dealTypes[i]) != -1) {
          this.setStateToTrue(this.state.dealTypes[i]);
        }
      }

    this.props.dealTypesFilter(filtersObj);
  }

  setStateToTrue = name => {
    this.setState({
      [name]: true
    });
  };

  steerWheeldealTypeHandle(dealType) {
    filtersObj.isFiltered = true;

    this.setState({
      updated: true
    });

    this.setState(
      {
        [dealType]: !this.state[dealType]
      },
      function() {
        filterHandle("dealTypes", dealType, this.state[dealType]);
        this.props.dealTypesFilter(filtersObj);
      }
    );
  }

  render() {
    var i = 1;
    return (
      <div className="filter">
        <div className="dealTypes-block-title">
          <FormattedMessage
            id="home.filter.dealType"
            defaultMessage="Deal type"
          />
        </div>
        <div className="block-container">
          {this.state.dealTypes.length > 0
            ? this.state.dealTypes.map(dealType => (
                <div key={dealType} className="filter-option">
                  <input
                    id={"dealType" + i}
                    type="checkbox"
                    onChange={() => this.steerWheeldealTypeHandle(dealType)}
                    checked={this.state[dealType]}
                  />
                  <label for={"dealType" + i++} className="filter-option-name">
                    <FormattedMessage
                      id={"home.filter.dealType." + toLowerCaseV2(dealType)}
                      defaultMessage={dealType}
                    />
                  </label>
                </div>
              ))
            : ""}
        </div>
      </div>
    );
  }

  bodyStyleToLowerCase(bodyStyle) {
    let b = bodyStyle;
    b = b.replace(" ", "_");

    return b.toLowerCase();
  }
}

export default DealType;
