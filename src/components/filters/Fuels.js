import React, { Component } from "react";
import filtersObj from "../../FiltersObj";
import { get } from "../../GetRequest";
import { setData, filterHandle } from "../../Util";
import $ from "jquery";
import "./../../filters.css";
import { FormattedMessage } from "react-intl";
import { toLowerCaseV2 } from "../../Util";

class Fuels extends Component {
  constructor(props) {
    super(props);

    this.state = {
      fuels: [],
    };
  }

  componentDidMount() {
    if (filtersObj.fuels.length > 0) {
      $(".fuels-block-title")
        .addClass("fuels-block-active")
        .next()
        .slideToggle(150);
    }
    $(document).ready(function () {
      $(".fuels-block-title").click(function (event) {
        $(this).toggleClass("fuels-block-active").next().slideToggle(150);
      });
    });

    get("fuels")
      .then((Response) => {
        this.setState(
          {
            fuels: Response.data,
          },
          function () {
            setData("fuels", this.setStateToTrue.bind(this));
          }
        );
      })
      .catch((error) => {
        if (error.response) {
          console.log(error.response.data);
        }
      });
    this.props.fuelsFilter(filtersObj);
  }

  setStateToTrue(name) {
    console.log(name + "  to true");
    this.setState({
      [name]: true,
    });
  }

  fuelHandle(fuelName) {
    filtersObj.isFiltered = true;

    this.setState({
      updated: true,
    });

    this.setState(
      {
        [fuelName]: !this.state[fuelName],
      },
      function () {
        console.log("fuels");
        console.log(fuelName);
        console.log(this.state[fuelName]);
        filterHandle("fuels", fuelName, this.state[fuelName]);
        this.props.fuelsFilter(filtersObj);
      }
    );
  }

  render() {
    return (
      <div className="filter">
        <div className="fuels-block-title">
          <FormattedMessage id="home.filter.fuels" defaultMessage="Fuels" />
        </div>
        <div className="block-container">
          {this.state.fuels.length > 0
            ? this.state.fuels.map((fuel) => (
                <div key={fuel} className="filter-option">
                  <input
                    id={fuel}
                    type="checkbox"
                    onChange={() => this.fuelHandle(this.fuelToLowerCase(fuel))}
                    checked={this.state[this.fuelToLowerCase(fuel)]}
                  />
                  <label for={fuel} className="filter-option-name">
                    <FormattedMessage
                      id={"home.filter.fuels." + toLowerCaseV2(fuel)}
                      defaultMessage={fuel}
                    />
                  </label>
                </div>
              ))
            : ""}
        </div>
      </div>
    );
  }

  fuelToLowerCase(fuel) {
    let f = fuel;
    f = f.replace("-", "_");
    f = f.replace(" ", "_");

    return f.toLowerCase();
  }
}

export default Fuels;
