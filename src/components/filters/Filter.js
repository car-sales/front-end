import React, { Component } from "react";
import Brand from "./Brand";
import Country from "./Country";
import Fuels from "./Fuels";
import BodyStyle from "./BodyStyle";
import Traction from "./Traction";
import Transmission from "./Transmission";
import InteriorColor from "./InteriorColor";
import ExteriorColor from "./ExteriorColor";
import DoorsNumber from "./DoorsNumber";
import Price from "./Price";
import Year from "./Year";
import Comfort from "./Comfort";
import Security from "./Security";
import Capacity from "./Capacity";
import HorsePower from "./HorsePower";
import State from "./State";
import Mileage from "./Mileage";
import DealType from "./DealType";
import $ from "jquery";
import SteerWheelSide from "./SteerWheelSide";
import { FormattedMessage } from "react-intl";
import { getRangeNames } from "../../Locale";

class Filter extends Component {
  constructor(props) {
    super(props);

    this.state = {
      rangeNames: { from: "from", to: "to" }
    };
  }

  componentDidMount() {
    $(document).ready(function() {
      $(".filter-btn").click(function(event) {
        $(".filters").toggleClass("active");
      });
    });

    $(document).ready(function() {
      $(".bttm-filter-btn").click(function(event) {
        $(".filters").toggleClass("active");
      });
    });

    var rangeNames = getRangeNames();

    if (rangeNames) {
      this.setState({
        rangeNames: { from: rangeNames.from, to: rangeNames.to }
      });
    }
  }

  render() {
    return (
      <div className="filters">
        <span className="filter-container-name">
          <FormattedMessage id="home.filter" defaultMessage="Filter" />
        </span>
        <div className="filter-btn" title="Close filters">
          <i className="fas fa-filter"></i>
        </div>
        <DealType dealTypesFilter={this.props.dealTypesFilter} />
        <State statesFilter={this.props.statesFilter} />
        <Price
          priceFilter={this.props.priceFilter}
          rangeNames={this.state.rangeNames}
        />
        <Year
          yearFilter={this.props.yearFilter}
          rangeNames={this.state.rangeNames}
        />
        <Brand
          brandsFilter={this.props.brandsFilter}
          modelsFilter={this.props.modelsFilter}
        />
        <Fuels fuelsFilter={this.props.fuelsFilter} />
        <Capacity
          capacityFilter={this.props.capacityFilter}
          rangeNames={this.state.rangeNames}
        />
        <HorsePower
          horsePowerFilter={this.props.horsePowerFilter}
          rangeNames={this.state.rangeNames}
        />
        <Mileage
          mileageFilter={this.props.mileageFilter}
          rangeNames={this.state.rangeNames}
        />
        <BodyStyle bodyStylesFilter={this.props.bodyStylesFilter} />
        <Traction tractionsFilter={this.props.tractionsFilter} />
        <Transmission transmissionsFilter={this.props.transmissionsFilter} />
        <InteriorColor interiorColorsFilter={this.props.interiorColorsFilter} />
        <ExteriorColor exteriorColorsFilter={this.props.exteriorColorsFilter} />
        <DoorsNumber doorsNumbersFilter={this.props.doorsNumbersFilter} />
        <SteerWheelSide
          steerWheelSidesFilter={this.props.steerWheelSidesFilter}
        />
        <Comfort comfortsFilter={this.props.comfortsFilter} />
        <Security securitiesFilter={this.props.securitiesFilter} />
        <Country
          countriesFilter={this.props.countriesFilter}
          regionsFilter={this.props.regionsFilter}
        />
        <div className="bttm-filter-btn" title="Close filters">
          <i className="fas fa-filter"></i>
          <FormattedMessage id="home.filter.close" defaultMessage=" close" />
        </div>
      </div>
    );
  }
}

export default Filter;
