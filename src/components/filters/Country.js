import React, { Component } from "react";
import Region from "./Region";
import filtersObj from "../../FiltersObj";
import { get } from "../../GetRequest";
import { setData, filterHandle } from "../../Util";
import $ from "jquery";
import "./../../filters.css";
import { FormattedMessage } from "react-intl";

class Country extends Component {
  constructor(props) {
    super(props);

    this.state = {
      countries: [],
      regions: [],
    };
  }

  componentDidMount() {
    if (filtersObj.countries.length > 0 || filtersObj.regions.length > 0) {
      $(".countries-block-title")
        .addClass("countries-block-active")
        .next()
        .slideToggle(150);
    }

    $(document).ready(function () {
      $(".countries-block-title").click(function (event) {
        $(this).toggleClass("countries-block-active").next().slideToggle(150);
      });
    });

    get("countries")
      .then((Response) => {
        this.setState(
          {
            countries: Response.data,
          },
          function () {
            var countries = [];
            this.state.countries.map((country) => {
              countries.push(country.countryName);
            });

            setData("countries", this.setStateToTrue);

            //regions

            this.state.countries.map((country) => {
              var regions = [];

              country.regions.map((region) => {
                regions.push(region.regionName);
              });

              setData("regions", regions, this.setStateToTrue);
            });

            //regions
          }
        );
      })
      .catch((error) => {
        if (error.response) {
        }
      });

    this.props.countriesFilter(filtersObj);
    this.props.regionsFilter(filtersObj);
  }

  setStateToTrue = (name) => {
    this.setState(
      {
        [name]: true,
      },
      function () {}
    );
  };

  countryHandle = (countryName) => {
    filtersObj.isFiltered = true;

    this.setState({
      updated: true,
    });

    this.setState(
      {
        [countryName]: !this.state[countryName],
      },
      function () {
        if (this.state[countryName]) {
          this.state.countries.map((country) => {
            if (countryName == country.countryName) {
              for (let i = 0; i < country.regions.length; i++) {
                var idx = filtersObj.regions.indexOf(
                  country.regions[i].regionName
                );

                if (idx != -1) {
                  filtersObj.regions.splice(idx, 1);
                  this.setState({
                    [country.regions[i].regionName]: false,
                  });
                }
              }
            }
          });
        }
        filterHandle("countries", countryName, this.state[countryName]);
        this.props.countriesFilter(filtersObj);
        this.props.regionsFilter(filtersObj);
      }
    );
  };

  regionHandle = (regionName, countryName) => {
    this.setState(
      {
        [regionName]: !this.state[regionName],
        [countryName]: false,
      },
      function () {
        filterHandle("regions", regionName, this.state[regionName]);
        filterHandle("countries", countryName, this.state[countryName]);
        this.props.regionsFilter(filtersObj);
        this.props.countriesFilter(filtersObj);
      }
    );
  };
  render() {
    return (
      <div className="filter">
        <div className="countries-block-title">
          <FormattedMessage
            id="home.filter.countries"
            defaultMessage="Country"
          />
        </div>
        <div className="block-container countries-block-container">
          {this.state.countries.length > 0
            ? this.state.countries.map((country) => (
                <div key={country.countryName} className="filter-option">
                  <Region
                    countryChecked={this.state}
                    countriesFilter={this.props.countriesFilter}
                    country={country}
                    countryHandle={this.countryHandle}
                    regionHandle={this.regionHandle}
                  />
                </div>
              ))
            : ""}
        </div>
      </div>
    );
  }

  fuelToLowerCase(fuel) {
    let f = fuel;
    try {
      f = f.replace("-", "_");
      f = f.replace(" ", "_");
      return f.toLowerCase();
    } catch (error) {
      return f;
    }
  }
}

export default Country;
