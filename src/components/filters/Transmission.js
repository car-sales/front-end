import React, { Component } from "react";
import filtersObj from "../../FiltersObj";
import { get } from "../../GetRequest";
import { setData, filterHandle } from "../../Util";
import { FormattedMessage } from "react-intl";
import $ from "jquery";
import "./../../filters.css";
import { toLowerCaseV2 } from "../../Util";

class transmission extends Component {
  constructor(props) {
    super(props);

    this.state = {
      transmissions: [],
    };
  }

  componentDidMount() {
    if (filtersObj.transmissions.length > 0) {
      $(".transmissions-block-title")
        .addClass("transmissions-block-active")
        .next()
        .slideToggle(150);
    }
    $(document).ready(function () {
      $(".transmissions-block-title").click(function (event) {
        $(this)
          .toggleClass("transmissions-block-active")
          .next()
          .slideToggle(150);
      });
    });

    get("transmissions")
      .then((Response) => {
        this.setState(
          {
            transmissions: Response.data,
          },
          function () {
            setData("transmissions", this.setStateToTrue);
          }
        );
      })
      .catch((error) => {
        if (error.response) {
        }
      });
    this.props.transmissionsFilter(filtersObj);
  }

  setStateToTrue = (name) => {
    this.setState({
      [name]: true,
    });
  };

  transmissionHandle(transmission) {
    filtersObj.isFiltered = true;

    this.setState({
      updated: true,
    });

    this.setState(
      {
        [transmission]: !this.state[transmission],
      },
      function () {
        filterHandle("transmissions", transmission, this.state[transmission]);
        this.props.transmissionsFilter(filtersObj);
      }
    );
  }

  render() {
    return (
      <div className="filter">
        <div className="transmissions-block-title">
          <FormattedMessage
            id="home.filter.transmissions"
            defaultMessage="Transmissions"
          />
        </div>
        <div className="block-container">
          {this.state.transmissions.length > 0
            ? this.state.transmissions.map((transmission) => (
                <div key={transmission} className="filter-option">
                  <input
                    id={transmission}
                    type="checkbox"
                    onChange={() =>
                      this.transmissionHandle(this.toLowerCase(transmission))
                    }
                    checked={this.state[this.toLowerCase(transmission)]}
                  />
                  <label for={transmission} className="filter-option-name">
                    <FormattedMessage
                      id={
                        "home.filter.transmissions." +
                        toLowerCaseV2(transmission)
                      }
                      defaultMessage={transmission}
                    />
                  </label>
                </div>
              ))
            : ""}
        </div>
      </div>
    );
  }

  toLowerCase(transmission) {
    let t = transmission;

    t = t.replace(" ", "_");

    return t.toLowerCase();
  }
}

export default transmission;
