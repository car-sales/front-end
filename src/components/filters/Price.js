import React, { Component } from "react";
import filtersObj from "../../FiltersObj";
import { get } from "../../GetRequest";
import "./../../filters.css";
import { FormattedMessage } from "react-intl";

class Price extends Component {
  constructor(props) {
    super(props);

    this.state = {
      priceStart: "",
      priceEnd: "",
      currencies: [],
      currency: "",
      negotiable: true
    };
  }

  componentDidMount() {
    get("currencies")
      .then(Response => {
        this.setState({
          currencies: Response.data
        });
      })
      .catch(error => {
        if (error.response) {
        }
      });
    this.setState({
      priceStart: filtersObj.priceStart > 0 ? filtersObj.priceStart : "",
      priceEnd:
        filtersObj.priceEnd < Math.pow(10, 7) ? filtersObj.priceEnd : "",
      negotiable: filtersObj.negotiable,
      currency: filtersObj.currency
    });
    this.props.priceFilter(filtersObj);
  }

  priceHandle = event => {
    filtersObj.isFiltered = true;

    var priceObject = event.target;
    switch (priceObject.name) {
      case "priceStart": {
        if (priceObject.valueAsNumber != priceObject.valueAsNumber) {
          filtersObj[priceObject.name] = 0;
        } else filtersObj[priceObject.name] = priceObject.valueAsNumber;

        break;
      }
      case "priceEnd": {
        if (priceObject.valueAsNumber != priceObject.valueAsNumber) {
          filtersObj[priceObject.name] = Math.pow(10, 7);
        } else filtersObj[priceObject.name] = priceObject.valueAsNumber;

        break;
      }
    }

    this.props.priceFilter(filtersObj);
  };

  currencyHandle = event => {
    filtersObj.isFiltered = true;

    filtersObj.currency = event.target.value;
    this.props.priceFilter(filtersObj);
  };

  negotiableHandler = event => {
    filtersObj.isFiltered = true;

    filtersObj.negotiable = !this.state.negotiable;
    this.setState(
      {
        negotiable: !this.state.negotiable
      },
      function() {
        this.props.priceFilter(filtersObj);
      }
    );
  };

  priceChangeHandler = e => {
    this.setState({
      [e.target.name]: e.target.valueAsNumber
    });
  };

  render() {
    return (
      <div className="filter">
        <div className="price-block-title">
          <FormattedMessage id="home.filter.price" defaultMessage="Price" />
        </div>
        <div className="filter-option">
          <FormattedMessage
            id="home.filter.from"
            defaultMessage={
              <input
                className="decimal-from-option"
                name="priceStart"
                type="number"
                placeholder="from:"
                min="0"
                max={Math.pow(10, 7)}
                onBlur={this.priceHandle}
                value={this.state.priceStart}
                onChange={this.priceChangeHandler}
              />
            }
          >
            {value => (
              <input
                className="decimal-from-option"
                name="priceStart"
                type="number"
                placeholder={`${value}:`}
                min="0"
                max={Math.pow(10, 7)}
                onBlur={this.priceHandle}
                value={this.state.priceStart}
                onChange={this.priceChangeHandler}
              />
            )}
          </FormattedMessage>

          <FormattedMessage
            id="home.filter.to"
            defaultMessage={
              <input
                className="decimal-to-option"
                name="priceEnd"
                type="number"
                placeholder="to:"
                min="0"
                max={Math.pow(10, 7)}
                onBlur={this.priceHandle}
                value={this.state.priceEnd}
                onChange={this.priceChangeHandler}
              />
            }
          >
            {value => (
              <input
                className="decimal-to-option"
                name="priceEnd"
                type="number"
                placeholder={`${value}:`}
                min="0"
                max={Math.pow(10, 7)}
                onBlur={this.priceHandle}
                value={this.state.priceEnd}
                onChange={this.priceChangeHandler}
              />
            )}
          </FormattedMessage>

          <select
            className="price-option-currency"
            onChange={this.currencyHandle}
          >
            {this.getCurrencies()}
          </select>
          <div className="negotiable-option">
            <label for="negotiable">
              <FormattedMessage
                id="home.filter.price.negotiablePrice"
                defaultMessage="Negotiable price"
              />
            </label>
            <input
              id="negotiable"
              type="checkbox"
              checked={this.state.negotiable ? 1 : 0}
              value="negotiable"
              onChange={this.negotiableHandler}
            />
          </div>
        </div>
      </div>
    );
  }

  getCurrencies() {
    var currencyOptions = [];
    if (this.state.currencies.length > 0) {
      this.state.currencies.map(currency => {
        var cn = "";
        switch (filtersObj.currency) {
          case "eur":
            cn = "€";
            break;
          case "usd":
            cn = "$";
            break;
          case "ron":
            cn = "RON";
            break;
          case "mdl":
            cn = "MDL";
            break;
        }

        if (cn == currency) {
          currencyOptions.push(
            <option
              key={currency}
              value={this.getCurrencyNameBySymbol(cn)}
              selected
            >
              {cn}
            </option>
          );
        } else {
          currencyOptions.push(
            <option
              key={currency}
              value={this.getCurrencyNameBySymbol(currency)}
            >
              {currency}
            </option>
          );
        }
      });
    }

    return currencyOptions;
  }

  getCurrencyNameBySymbol(cn) {
    switch (cn) {
      case "€":
        return "eur";
      case "$":
        return "usd";
      case "RON":
        return "ron";
      case "MDL":
        return "mdl";
    }
  }
}

export default Price;
