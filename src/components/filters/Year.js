import React, { Component } from "react";
import filtersObj from "../../FiltersObj";
import { get } from "../../GetRequest";
import { setData, filterHandle } from "../../Util";
import $ from "jquery";
import "./../../filters.css";
import { FormattedMessage } from "react-intl";
import { getRangeNames } from "../../Locale";

class Year extends Component {
  constructor(props) {
    super(props);

    this.state = {
      from: "from",
      to: "to",
      yearStart: "",
      yearEnd: ""
    };
  }

  componentDidMount() {
    this.setState({
      yearStart: filtersObj.yearStart > 1900 ? filtersObj.yearStart : "",
      yearEnd:
        filtersObj.yearEnd < new Date().getFullYear + 1
          ? filtersObj.yearEnd
          : ""
    });

    var rangeNames = getRangeNames();

    if (rangeNames) {
      this.setState({
        from: rangeNames.from,
        to: rangeNames.to
      });
    }
    this.props.yearFilter(filtersObj);
  }

  yearHandle = event => {
    filtersObj.isFiltered = true;

    var yearObject = event.target;
    switch (yearObject.name) {
      case "yearStart": {
        if (yearObject.valueAsNumber != yearObject.valueAsNumber) {
          filtersObj[yearObject.name] = 1900;
        } else filtersObj[yearObject.name] = yearObject.valueAsNumber;

        break;
      }
      case "yearEnd": {
        if (yearObject.valueAsNumber != yearObject.valueAsNumber) {
          filtersObj[yearObject.name] = new Date().getFullYear() + 1;
        } else filtersObj[yearObject.name] = yearObject.valueAsNumber;

        break;
      }
    }

    this.props.yearFilter(filtersObj);
  };

  yearChangeHandler = e => {
    this.setState({
      [e.target.name]: e.target.valueAsNumber
    });
  };

  render() {
    return (
      <div className="filter">
        <div className="year-block-title">
          <FormattedMessage id="home.filter.year" defaultMessage="Year" />
        </div>
        <div className="filter-option">
          <FormattedMessage
            id="home.filter.from"
            defaultMessage={
              <input
                className="decimal-from-option"
                name="yearStart"
                type="number"
                placeholder="from:"
                min="1900"
                max={new Date().getFullYear() + 1}
                onBlur={this.yearHandle}
                value={this.state.yearStart}
                onChange={this.yearChangeHandler}
              />
            }
          >
            {value => (
              <input
                className="decimal-from-option"
                name="yearStart"
                type="number"
                placeholder={`${value}:`}
                min="1900"
                max={new Date().getFullYear() + 1}
                onBlur={this.yearHandle}
                value={this.state.yearStart}
                onChange={this.yearChangeHandler}
              />
            )}
          </FormattedMessage>
          <FormattedMessage
            id="home.filter.to"
            defaultMessage={
              <input
                className="decimal-to-option"
                name="yearEnd"
                type="number"
                placeholder="to:"
                min="1900"
                max={new Date().getFullYear() + 1}
                onBlur={this.yearHandle}
                value={this.state.yearEnd}
                onChange={this.yearChangeHandler}
              />
            }
          >
            {value => (
              <input
                className="decimal-to-option"
                name="yearEnd"
                type="number"
                placeholder={`${value}:`}
                min="1900"
                max={new Date().getFullYear() + 1}
                onBlur={this.yearHandle}
                value={this.state.yearEnd}
                onChange={this.yearChangeHandler}
              />
            )}
          </FormattedMessage>
        </div>
      </div>
    );
  }
}

export default Year;
