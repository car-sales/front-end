import React, { Component } from "react";
import Model from "./Model";
import filtersObj from "../../FiltersObj";
import { get } from "../../GetRequest";
import { setData, filterHandle } from "../../Util";
import $ from "jquery";
import "./../../filters.css";
import { FormattedMessage } from "react-intl";

class Brand extends Component {
  constructor(props) {
    super(props);

    this.state = {
      brands: [],
      models: [],
    };
  }

  componentDidMount() {
    if (filtersObj.brands.length > 0 || filtersObj.models.length > 0) {
      $(".brands-block-title")
        .addClass("brands-block-active")
        .next()
        .slideToggle(150);
    }

    $(document).ready(function () {
      $(".brands-block-title").click(function (event) {
        $(this).toggleClass("brands-block-active").next().slideToggle(150);
      });
    });

    get("brands")
      .then((Response) => {
        this.setState(
          {
            brands: Response.data,
          },
          function () {
            var brands = [];
            this.state.brands.map((brand) => {
              brands.push(brand.brandName);
            });

            setData("brands", this.setStateToTrue);

            //models

            this.state.brands.map((brand) => {
              var models = [];

              brand.models.map((model) => {
                models.push(model.modelName);
              });

              setData("models", this.setStateToTrue);
            });
          }
        );
      })
      .catch((error) => {
        if (error.response) {
        }
      });

    this.props.brandsFilter(filtersObj);
    this.props.modelsFilter(filtersObj);
  }

  setStateToTrue = (name) => {
    this.setState(
      {
        [name]: true,
      },
      function () {}
    );
  };

  brandHandle = (brandName) => {
    this.setState({
      updated: true,
    });

    this.setState(
      {
        [brandName]: !this.state[brandName],
      },
      function () {
        filtersObj.isFiltered = true;

        if (this.state[brandName]) {
          this.state.brands.map((brand) => {
            if (brandName == brand.brandName) {
              for (let i = 0; i < brand.models.length; i++) {
                var idx = filtersObj.models.indexOf(brand.models[i].modelName);

                if (idx != -1) {
                  filtersObj.models.splice(idx, 1);
                  this.setState({
                    [brand.models[i].modelName]: false,
                  });
                }
              }
            }
          });
        }
        filterHandle("brands", brandName, this.state[brandName]);
        this.props.brandsFilter(filtersObj);
        this.props.modelsFilter(filtersObj);
      }
    );
  };

  modelHandle = (modelName, brandName) => {
    this.setState(
      {
        [modelName]: !this.state[modelName],
        [brandName]: false,
      },
      function () {
        filtersObj.isFiltered = true;
        filterHandle("models", modelName, this.state[modelName]);
        filterHandle("brands", brandName, this.state[brandName]);
        this.props.modelsFilter(filtersObj);
        this.props.brandsFilter(filtersObj);
      }
    );
  };
  render() {
    return (
      <div className="filter">
        <div className="brands-block-title">
          <FormattedMessage id="home.filter.brands" defaultMessage="Brands" />
        </div>
        <div className="block-container brands-block-container">
          {this.state.brands.length > 0
            ? this.state.brands.map((brand) => (
                <div key={brand.brandName} className="filter-option">
                  <Model
                    brandChecked={this.state}
                    brandsFilter={this.props.brandsFilter}
                    brand={brand}
                    brandHandle={this.brandHandle}
                    modelHandle={this.modelHandle}
                  />
                </div>
              ))
            : ""}
        </div>
      </div>
    );
  }

  fuelToLowerCase(fuel) {
    let f = fuel;
    try {
      f = f.replace("-", "_");
      f = f.replace(" ", "_");
      return f.toLowerCase();
    } catch (error) {
      return f;
    }
  }
}

export default Brand;
