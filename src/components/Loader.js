import React from "react";
import Loader from "react-loader-spinner";

export default props => {
  return (
    <div className={"loader " + props.classes}>
      <Loader type="Bars" color="#14517c" height={50} width={50} />
    </div>
  );
};
