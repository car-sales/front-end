import React, { Component } from "react";
import axios from "axios";
import Brand from "./adProperties/carProperties/Brand";
import Model from "./adProperties/carProperties/Model";
import BodyStyle from "./adProperties/carProperties/BodyStyle";
import DoorsNumber from "./adProperties/carProperties/DoorsNumber";
import State from "./adProperties/carProperties/State";
import SteerWheelSide from "./adProperties/carProperties/SteerWheelSide";
import Traction from "./adProperties/carProperties/Traction";
import Transmission from "./adProperties/carProperties/Transmission";
import Mileage from "./adProperties/carProperties/Mileage";
import Color from "./adProperties/carProperties/Color";
import EngineProperties from "./adProperties/carProperties/engineProperties/EngineProperties";
import Security from "./adProperties/carProperties/Security";
import Comfort from "./adProperties/carProperties/Comfort";
import DealType from "./adProperties/DealType";
import Phone from "./adProperties/Phone";
import Email from "./adProperties/Email";
import Description from "./adProperties/Description";
import Price from "./adProperties/Price";
import Country from "./adProperties/Country";
import Region from "./adProperties/Region";
import Year from "./adProperties/carProperties/Year";
import Image from "./adProperties/Image";
import { API_URL } from "../Constants";
import { get } from "../GetRequest";
import "./../selectList.css";
import "./../adForm.css";
import { getCookie } from "../CookieManager";
import { FormattedMessage } from "react-intl";
import { connect } from "react-redux";
import {
  showDisabled,
  hideDisabled,
  optionDisable,
  optionEnable,
} from "./../redux/actions";
import FormWidget from "./adProperties/FormWidget";

class AddAnAd extends Component {
  constructor(props) {
    super(props);
    this.state = {
      optionsList: [
        "Deal type",
        "Brand",
        "Model",
        "Body style",
        "Doors number",
        "State",
        "Steer wheel",
        "Traction",
        "Transmission",
        "Mileage",
        "Exterior color",
        "Interior color",
        "Year",
        "Fuel",
        "Capacity",
        "Horse power",
        "Price",
        "Contacts",
        "Region",
        "Photos",
      ],
      brands: [],
      models: [],
      bodyStyles: [],
      traction: [],
      colors: [],
      fuels: [],
      securities: [],
      comforts: [],
      transmissions: [],
      currencies: [],
      countries: [],
      regions: [],
      chosenBrandId: "",
      chosenCountryId: "",
      images: [],
      modelSelected: false,
      modelId: 0,
      fuel: "",
      capacity: 0,
      horsePower: 0,
      interiorColor: "",
      exteriorColor: "",
      comfortsIds: [],
      securitiesIds: [],
      currency: "",
      price: null,
      negotiable: false,
      mileage: 0,
      dealType: "",
      state: "",
      bodyStyle: "",
      doorsNumber: 0,
      tractionName: "",
      transmission: "",
      steerWheelSide: "",
      year: 0,
      description: "",
      phone: 0,
      email: "",
      regionId: 0,
    };
  }

  submitHandler = (e) => {
    e.preDefault();

    if (this.state.securities.length < 1) {
      window.alert("Please select at least 1 security.");
      return;
    }

    if (this.state.comforts.length < 1) {
      window.alert("Please select at least 1 comfort.");
      return;
    }

    this.props.showDisabled();

    const files = this.state.images;
    const formData = new FormData();
    files.forEach((file, i) => {
      formData.append("files", file);
    });

    let adObject = {
      car: {
        model: {
          id: this.state.modelId,
        },
        engine: {
          fuel: this.state.fuel,
          capacity: this.state.capacity,
          horsePower: this.state.horsePower,
        },
        year: this.state.year,
        interiorColor: this.state.interiorColor,
        exteriorColor: this.state.exteriorColor,
        mileage: this.state.mileage,
        state: this.state.state,
        bodyStyle: this.state.bodyStyle,
        doorsNumber: this.state.doorsNumber,
        traction: this.state.tractionName,
        transmission: this.state.transmission,
        steerWheelSide: this.state.steerWheelSide,
        securities: this.state.securitiesIds,
        comforts: this.state.comfortsIds,
      },
      dealType: this.state.dealType,
      price: !this.state.negotiable ? this.state.price : -1,
      currency: this.state.negotiable ? "EUR" : this.state.currency,
      negotiable: this.state.negotiable,
      phone: this.state.phone,
      email: this.state.email,
      region: {
        id: this.state.regionId,
      },
      description: this.state.description,
    };
    formData.append("ad", JSON.stringify(adObject));
    axios
      .post(`${API_URL}ad`, formData, {
        headers: {
          "content-type": "application/json",
          Authorization: getCookie("access_token"),
        },
      })
      .then((Response) => {
        axios
          .get(`${API_URL}current-user`, {
            params: { token: getCookie("access_token") },
            headers: {
              Authorization: getCookie("access_token"),
            },
          })
          .then((Response) => {
            const userData = Response.data;

            localStorage.setItem("user", JSON.stringify(userData));
          })
          .catch((error) => {
            if (error.response) {
            }
          });
        this.props.hideDisabled();

        window.alert("Ad successfully created.");
      })
      .catch((error) => {
        if (error.response) {
          this.props.hideDisabled();
          window.alert("Error on creating ad.");
        }
      });
  };

  componentDidMount() {
    this.getRequest("brands");
    this.getRequest("currencies");
    this.getRequest("countries");
    this.getRequest("securities");
    this.getRequest("comforts");
    this.getRequest("fuels");
  }

  getRequest(route) {
    get(route)
      .then((Response) => {
        this.setState({
          [route]: Response.data,
        });
      })
      .catch((error) => {
        if (error.response) {
        }
      });
  }

  countrySelectHandle = (e) => {
    this.setState({
      chosenCountryId: e.target.value,
    });

    var regions;
    this.state.countries.map((country) => {
      if (country.id == e.target.value) {
        regions = country.regions;
      }
    });

    if (regions != null) {
      this.setState({
        regions: regions,
      });
    } else {
      this.setState({
        regions: [],
      });
    }
  };

  brandSelectHandle = (e) => {
    if (e.target.value === "") this.props.optionDisable("option2");
    else this.props.optionEnable("option2");
    this.setState({ chosenBrandId: e.target.value, modelSelected: false });

    var models;
    this.state.brands.map((brand) => {
      if (brand.id == e.target.value) {
        models = brand.models;
      }
    });

    if (models != null) {
      this.setState({
        models: models,
      });
    } else {
      this.setState({
        models: [],
      });
    }
  };

  modelSelectHandler = (e) => {
    if (e.target.value === "") this.props.optionDisable("option3");
    else this.props.optionEnable("option3");
    this.setState({
      modelSelected: e.target.value == "" ? false : true,
      modelId: e.target.value,
    });

    this.getRequest("bodyStyles");
    this.getRequest("traction");
    this.getRequest("transmissions");
    this.getRequest("colors");
    this.getRequest("fuels");
    this.getRequest("securities");
    this.getRequest("comforts");
  };

  bodyStyleSelectHandler = (e) => {
    if (e.target.value === "") this.props.optionDisable("option4");
    else this.props.optionEnable("option4");
    this.setState({
      bodyStyle: e.target.value,
    });
  };

  doorsNumberSelectHandler = (e) => {
    if (e.target.value === "") this.props.optionDisable("option5");
    else this.props.optionEnable("option5");
    this.setState({
      doorsNumber: e.target.value,
    });
  };
  stateSelectHandler = (e) => {
    if (e.target.value === "") this.props.optionDisable("option6");
    else this.props.optionEnable("option6");
    this.setState({
      state: e.target.value,
    });
  };
  steerWheelSideSelectHandler = (e) => {
    if (e.target.value === "") this.props.optionDisable("option7");
    else this.props.optionEnable("option7");
    this.setState({
      steerWheelSide: e.target.value,
    });
  };
  tractionSelectHandler = (e) => {
    if (e.target.value === "") this.props.optionDisable("option8");
    else this.props.optionEnable("option8");
    this.setState({
      tractionName: e.target.value,
    });
  };
  transmissionSelectHandler = (e) => {
    if (e.target.value === "") this.props.optionDisable("option9");
    else this.props.optionEnable("option9");
    this.setState({
      transmission: e.target.value,
    });
  };
  mileageInputHandler = (e) => {
    if (e.target.value === "") this.props.optionDisable("option10");
    else this.props.optionEnable("option10");
    this.setState({
      mileage: e.target.value,
    });
  };
  interiorColorSelectHandler = (e) => {
    if (e.target.value === "") this.props.optionDisable("option12");
    else this.props.optionEnable("option12");
    this.setState({
      interiorColor: e.target.value,
    });
  };
  exteriorColorSelectHandler = (e) => {
    if (e.target.value === "") this.props.optionDisable("option11");
    else this.props.optionEnable("option11");
    this.setState({
      exteriorColor: e.target.value,
    });
  };
  capacityInputHandler = (e) => {
    if (e.target.value === "") this.props.optionDisable("option15");
    else this.props.optionEnable("option15");
    this.setState({
      capacity: e.target.value,
    });
  };
  fuelSelectHandler = (e) => {
    if (e.target.value === "") this.props.optionDisable("option14");
    else this.props.optionEnable("option14");
    this.setState({
      fuel: e.target.value,
    });
  };
  horsePowerInputHandler = (e) => {
    if (e.target.value === "") this.props.optionDisable("option16");
    else this.props.optionEnable("option16");
    this.setState({
      horsePower: e.target.value,
    });
  };
  priceInputHandler = (e) => {
    this.setState({
      price: e.target.value,
    });
  };
  descriptionInputHandler = (e) => {
    this.setState({
      description: e.target.value,
    });
  };
  phoneInputHandler = (e) => {
    this.setState({
      phone: e.target.value,
    });
  };
  emailInputHandler = (e) => {
    this.setState({
      email: e.target.value,
    });
  };
  regionSelectHandler = (e) => {
    if (e.target.value === "") this.props.optionDisable("option19");
    else this.props.optionEnable("option19");
    this.setState({
      regionId: e.target.value,
    });
  };

  dealTypeSelectHandler = (e) => {
    if (e.target.value === "") this.props.optionDisable("option1");
    else this.props.optionEnable("option1");
    this.setState({
      dealType: e.target.value,
    });
  };
  currencySelectHandler = (e) => {
    this.setState(
      {
        currency: e.target.value,
      },
      function () {
        if (this.state.currency.length > 1) this.props.optionEnable("option17");
        else this.props.optionDisable("option17");
      }
    );
  };
  yearInputHandler = (e) => {
    if (e.target.value === "") this.props.optionDisable("option13");
    else this.props.optionEnable("option13");
    this.setState({
      year: e.target.value,
    });
  };

  comfortsHandler = (e) => {
    var id = e.target.value;
    var comfortsIds = this.state.comfortsIds;

    var idx = comfortsIds.findIndex((i) => i.id === id);
    if (idx !== -1) comfortsIds.splice(idx, 1);
    else {
      comfortsIds.push({ id: id });
      this.setState({
        comfortsIds: comfortsIds,
      });
    }
  };

  securitiesHandler = (e) => {
    var id = e.target.value;
    var securitiesIds = this.state.securitiesIds;

    var idx = securitiesIds.findIndex((i) => i.id === id);
    if (idx !== -1) securitiesIds.splice(idx, 1);
    else {
      securitiesIds.push({ id: id });
      this.setState({
        securitiesIds: securitiesIds,
      });
    }
  };

  negotiableHandler = (e) => {
    this.setState(
      {
        negotiable: !this.state.negotiable,
      },
      function () {
        if (this.state.negotiable) this.props.optionEnable("option17");
        else if (this.state.currency === "")
          this.props.optionDisable("option17");
        else this.props.optionEnable("opion17");
      }
    );
  };

  imageSelectedHandler = (e) => {
    var currentImages = this.state.images;
    var files = e.target.files;

    var exists;

    for (let i = 0; i < files.length; i++) {
      exists = false;
      if (this.state.images.length > 0)
        for (let j = 0; j < this.state.images.length; j++) {
          if (this.state.images[j].name == files[i].name) exists = true;
        }

      if (!exists) {
        exists = true;
        currentImages.push(files[i]);
      }
    }

    this.setState(
      {
        images: currentImages,
      },
      function () {
        if (!this.state.images.length) this.props.optionDisable("option20");
        else this.props.optionEnable("option20");
      }
    );
  };

  setImageForPreview = (name) => {
    var temp;
    var i = 0;
    const { images } = this.state;
    images.map((image) => {
      if (image.name === name) {
        temp = images[i];
        images[i] = images[0];
        images[0] = temp;
        this.setState({ images: images });
        return;
      }
      i++;
    });
  };

  removeImageHandler = (e) => {
    var imageName = e.target.attributes[0].value;
    var images = this.state.images;
    for (let i = 0; i < images.length; i++) {
      if (images[i].name == imageName) {
        images.splice(i, 1);
      }
    }

    this.setState(
      {
        images: images,
      },
      function () {
        if (!this.state.images.length) this.props.optionDisable("option20");
        else this.props.optionEnable("option20");
      }
    );
  };

  render() {
    return (
      <div className="ad-form">
        <form onSubmit={this.submitHandler}>
          <div className="input-container">
            <DealType dealTypeHandle={this.dealTypeSelectHandler} />
            <div className="first-options">{this.displayMain()}</div>
          </div>
          <div className="second-options">{this.secondOptions()}</div>
        </form>
        <FormWidget optionsList={this.state.optionsList} />
      </div>
    );
  }

  displayMain() {
    var c = [];

    c.push(
      <Brand brands={this.state.brands} brandHandle={this.brandSelectHandle} />
    );

    c.push(
      <Model models={this.state.models} modelHandle={this.modelSelectHandler} />
    );

    c.push(this.carProperties());

    return c;
  }

  carProperties() {
    var c = [];
    c.push(
      <BodyStyle
        bodyStyleHandle={this.bodyStyleSelectHandler}
        modelSelected={this.state.modelSelected}
        chosenBrandId={this.state.chosenBrandId}
        bodyStyles={this.state.bodyStyles}
      />
    );
    c.push(
      <DoorsNumber
        doorsNumberHandle={this.doorsNumberSelectHandler}
        modelSelected={this.state.modelSelected}
        chosenBrandId={this.state.chosenBrandId}
      />
    );
    c.push(
      <State
        stateHandle={this.stateSelectHandler}
        modelSelected={this.state.modelSelected}
        chosenBrandId={this.state.chosenBrandId}
      />
    );
    c.push(
      <SteerWheelSide
        steerWheelSideHandle={this.steerWheelSideSelectHandler}
        modelSelected={this.state.modelSelected}
        chosenBrandId={this.state.chosenBrandId}
      />
    );
    c.push(
      <Traction
        tractionHandle={this.tractionSelectHandler}
        traction={this.state.traction}
        modelSelected={this.state.modelSelected}
        chosenBrandId={this.state.chosenBrandId}
      />
    );
    c.push(
      <Transmission
        transmissionHandle={this.transmissionSelectHandler}
        transmissions={this.state.transmissions}
        modelSelected={this.state.modelSelected}
        chosenBrandId={this.state.chosenBrandId}
      />
    );
    c.push(
      <Mileage
        mileageHandle={this.mileageInputHandler}
        modelSelected={this.state.modelSelected}
        chosenBrandId={this.state.chosenBrandId}
      />
    );
    c.push(
      <Color
        colorHandle={this.exteriorColorSelectHandler}
        colorType="Exterior color"
        colors={this.state.colors}
        modelSelected={this.state.modelSelected}
        chosenBrandId={this.state.chosenBrandId}
      />
    );
    c.push(
      <Color
        colorHandle={this.interiorColorSelectHandler}
        colorType="Interior color"
        colors={this.state.colors}
        modelSelected={this.state.modelSelected}
        chosenBrandId={this.state.chosenBrandId}
      />
    );

    c.push(
      <Year
        yearHandle={this.yearInputHandler}
        modelSelected={this.state.modelSelected}
        chosenBrandId={this.state.chosenBrandId}
      />
    );

    return c;
  }

  secondOptions() {
    var c = [];
    c.push(
      <EngineProperties
        capacityHandle={this.capacityInputHandler}
        horsePowerHandle={this.horsePowerInputHandler}
        fuelHandle={this.fuelSelectHandler}
        fuels={this.state.fuels}
      />
    );
    c.push(
      <Security
        securitiesHandle={this.securitiesHandler}
        securities={this.state.securities}
        modelSelected={this.state.modelSelected}
        chosenBrandId={this.state.chosenBrandId}
      />
    );
    c.push(
      <Comfort
        comfortsHandle={this.comfortsHandler}
        comforts={this.state.comforts}
        modelSelected={this.state.modelSelected}
        chosenBrandId={this.state.chosenBrandId}
      />
    );
    c.push(
      <Price
        priceHandle={this.priceInputHandler}
        currencyHandle={this.currencySelectHandler}
        currencies={this.state.currencies}
        negotiableHandler={this.negotiableHandler}
        negotiable={this.state.negotiable}
        price={this.state.price}
        selectedCurrency={this.state.currency}
      />
    );
    c.push(<Description descriptionHandle={this.descriptionInputHandler} />);
    c.push(<Phone phoneHandle={this.phoneInputHandler} />);
    c.push(<Email emailHandle={this.emailInputHandler} />);
    c.push(
      <Country
        countries={this.state.countries}
        countryHandle={this.countrySelectHandle}
      />
    );
    c.push(
      <Region
        regionHandle={this.regionSelectHandler}
        regions={this.state.regions}
        chosenCountryId={this.state.chosenCountryId}
      />
    );
    c.push(
      <Image
        images={this.state.images}
        imageHandle={this.imageSelectedHandler}
        imageForPreview={this.setImageForPreview}
        removeImage={this.removeImageHandler}
      />
    );
    c.push(
      <button
        className="ad-submit"
        type="submit"
        disabled={this.props.app.disabled}
      >
        {this.props.app.disabled && <i class="fas fa-circle-notch fa-spin"></i>}
        &nbsp;
        <FormattedMessage id="contact.submit" defaultMessage="Submit" />
      </button>
    );

    return c;
  }
}

const mapDispatchToProps = {
  showDisabled,
  hideDisabled,
  optionDisable,
  optionEnable,
};

const mapStateToProps = (state) => state;

export default connect(mapStateToProps, mapDispatchToProps)(AddAnAd);
