import React from "react";
import { Link } from "react-router-dom";
import { FormattedMessage } from "react-intl";
import Popup from "reactjs-popup";

export default props => {
  var i = 1;
  return (
    <div key={props.ad.id} className="ads">
      <div className="user-ad-action">
        <span className="action">
          <i class="far fa-edit"></i>
          Edit
        </span>
        <Popup trigger={<span className="action">Delete</span>} modal>
          {close => (
            <div className="modal">
              <a className="close" onClick={close}>
                &times;
              </a>

              <div className="content">
                <img
                  src={`data:image/png;base64,${props.ad.image}`}
                  width="25%"
                  height="25%"
                />
                <br />
                <span className="modal-car-model">
                  {props.ad.brand}&nbsp;{props.ad.model}
                  <br />
                </span>
                <br />
                <span className="modal-car-price">
                  {props.ad.price == -1 ? (
                    <FormattedMessage
                      id="home.ads.negotiable"
                      defaultMessage="Negotiable"
                    />
                  ) : (
                    props.ad.price + " " + props.ad.currency
                  )}
                </span>
                <br />
                <span className="modal-question">
                  Are you sure you want to delete this ad?
                </span>
              </div>
              <div className="actions">
                <button
                  className="button"
                  onClick={() => {
                    props.delete(props.ad.id);
                  }}
                >
                  Yes
                </button>
                <button
                  className="button"
                  onClick={() => {
                    close();
                  }}
                >
                  No
                </button>
              </div>
            </div>
          )}
        </Popup>
      </div>
      <div className="ad-image">
        <a href={"ad/" + props.ad.id}>
          <img
            src={`data:image/png;base64,${props.ad.image}`}
            width="100%"
            height="100%"
          />
        </a>
      </div>
      <a href={"ad/" + props.ad.id}>
        {props.ad.brand}&nbsp;{props.ad.model}
      </a>
      <br />
      {props.ad.price == -1 ? (
        <FormattedMessage
          id="home.ads.negotiable"
          defaultMessage="Negotiable"
        />
      ) : (
        props.ad.price + " " + props.ad.currency
      )}
    </div>
  );
};
