import React from "react";
import Ad from "./Ad";
import UserAds from "./UserAds";
import SavedAds from "./SavedAds";

export default (props) => {
  switch (props.category) {
    case "ads":
      return props.ads.map((ad) => <Ad ad={ad} key={ad.id} />);
    case "userAds":
      return props.ads.map((ad) => (
        <UserAds ad={ad} key={ad.id} delete={props.delete} />
      ));
    case "savedAds":
      return props.ads.map((ad) => (
        <SavedAds ad={ad} key={ad.id} unlikeAd={props.unlikeAd} />
      ));
  }
};
