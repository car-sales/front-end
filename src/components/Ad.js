import React from "react";
import { Link } from "react-router-dom";
import { FormattedMessage } from "react-intl";

export default ({ ad }) => {
  return (
    <div key={ad.id} className="ads">
      <Link to={"ad/" + ad.id}>
        <div className="ad-image">
          <img
            src={`data:image/png;base64,${ad.image}`}
            width="100%"
            height="100%"
          />
        </div>
      </Link>
      <Link to={"ad/" + ad.id}>
        {ad.brand}&nbsp;{ad.model}
      </Link>
      <br />
      {price(ad.price)}&nbsp;
      {currency(ad.currency, ad.negotiable)}
    </div>
  );
};

const price = (value) => {
  return value < 1 ? "" : value;
};

const currency = (value, negotiable) => {
  return negotiable ? (
    <FormattedMessage id="home.ads.negotiable" defaultMessage="Negotiable" />
  ) : (
    value
  );
};
