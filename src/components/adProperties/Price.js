import React, { Component } from "react";
import { FormattedMessage } from "react-intl";

class Price extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div className="input-option">
        <div className="price">
          <FormattedMessage id="home.filter.price" defaultMessage="Price" />
          :&nbsp;
          {!this.props.negotiable ? (
            <input
              onChange={this.props.priceHandle}
              value={this.props.price}
              type="number"
            />
          ) : (
            <input
              onChange={this.props.priceHandle}
              value={this.props.price}
              type="number"
              disabled
            />
          )}
        </div>
        <div className="currency">
          <FormattedMessage id="ad.currency" defaultMessage="Currency" />
          :&nbsp;
          {this.props.price > 0 && !this.props.negotiable ? (
            <select onChange={this.props.currencyHandle}>
              <FormattedMessage
                id="ad.select"
                defaultMessage={<option value="">Select</option>}
              >
                {value => <option value="">{value}</option>}
              </FormattedMessage>
              {this.props.currencies.map(currency =>
                this.setCurrencyValue(currency) ===
                this.props.selectedCurrency ? (
                  <option selected value={this.props.selectedCurrency}>
                    {currency}
                  </option>
                ) : (
                  <option value={this.setCurrencyValue(currency)}>
                    {currency}
                  </option>
                )
              )}
            </select>
          ) : (
            <select disabled className="disabled-select">
              {this.props.selectedCurrency === "" ? (
                <FormattedMessage
                  id="ad.select"
                  defaultMessage={<option value="">Select</option>}
                >
                  {value => <option value="">{value}</option>}
                </FormattedMessage>
              ) : (
                <option value={this.props.selectedCurrency}>
                  {this.transformCurrencyValue(this.props.selectedCurrency)}
                </option>
              )}
            </select>
          )}
        </div>
        <div className="negotiable-price">
          <input
            id="negotiable-submit"
            onChange={this.props.negotiableHandler}
            type="checkbox"
          />
          <label for="negotiable-submit">
            <FormattedMessage
              id="home.ads.negotiable"
              defaultMessage="Negotiable"
            />
          </label>
        </div>
      </div>
    );
  }

  setCurrencyValue(currency) {
    switch (currency) {
      case "$":
        return "USD";
      case "€":
        return "EUR";
      default:
        return currency;
    }
  }

  transformCurrencyValue(currency) {
    switch (currency) {
      case "USD":
        return "$";
      case "EUR":
        return "€";
      default:
        return currency;
    }
  }
}

export default Price;
