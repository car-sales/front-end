import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import $ from "jquery";

class Image extends Component {
  constructor(props) {
    super(props);
    this.state = {
      images: []
    };
  }

  componentDidMount() {
    $(document).ready(function() {
      $(".for-preview-img").click(function(event) {
        $(".for-preview-img").toggleClass("active");
      });
    });
  }

  render() {
    return (
      <div className="input-option">
        <FormattedMessage id="ad.addPictures" defaultMessage="Add pictures" />
        :&nbsp;
        <label className="browse-images-btn" for="upload-photo">
          <FormattedMessage id="ad.select" defaultMessage="Select" />
          &nbsp;
        </label>
        <label for="upload-photo">
          &nbsp;{this.props.images.length}&nbsp;
          <FormattedMessage
            id="ad.imagesLoaded"
            defaultMessage="images loaded"
          />
          .
        </label>
        <input
          type="file"
          id="image"
          accept="image/png, image/jpeg,image/jpg"
          multiple
          onChange={this.props.imageHandle}
          required
          id="upload-photo"
        />
        <div className="chosen-images">
          {this.props.images && this.showImage()}
        </div>
      </div>
    );
  }

  showImage() {
    var i = 1;
    const images = [];
    var className = "";
    this.props.images.map(image => {
      className += "for-preview-img";
      className += " img" + i;

      if (i == 1) {
        className += " active";
      }

      images.push(
        <div className="image-block">
          <div
            name={image.name}
            id={"img" + i}
            className={className}
            onClick={this.showPreviewBtn}
          >
            For preview
          </div>
          <div
            onClick={this.props.removeImage}
            value={image.name}
            className="delete-image"
          >
            -
          </div>
          <img src={URL.createObjectURL(image)} width="100%" />
        </div>
      );
      className = "";
      ++i;
    });

    return images;
  }

  showPreviewBtn = e => {
    this.props.imageForPreview(e.target.attributes.name.value);
  };
}

export default Image;
