import React, { Component } from "react";

class Email extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div className="input-option">
        Email:&nbsp;
        {/* <b>*</b> */}
        <input onChange={this.props.emailHandle} type="text" required />
      </div>
    );
  }
}

export default Email;
