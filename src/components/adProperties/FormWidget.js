import React from "react";
import "./../../alerts.css";
import { useDispatch, useSelector } from "react-redux";
import { hideSignupSuccess } from "../../redux/actions";

export default ({ optionsList }) => {
  const stateOptions = useSelector(state => {
    return state.adForm;
  });

  var i = 1;

  return (
    <div className="form-widget">
      {optionsList.map(option => {
        return (
          <div
            className={
              stateOptions["option" + i]
                ? "option" + i++ + " active"
                : "option" + i++
            }
          >
            {option}
          </div>
        );
      })}
    </div>
  );
};
