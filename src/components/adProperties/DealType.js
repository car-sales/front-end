import React, { Component } from "react";
import { FormattedMessage } from "react-intl";

class DealType extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div className="input-option">
        <FormattedMessage
          id="home.filter.dealType"
          defaultMessage="Deal type"
        />
        <select onChange={this.props.dealTypeHandle}>
          <FormattedMessage
            id="ad.select"
            defaultMessage={<option value="">Select</option>}
          >
            {select => <option value="">{select}</option>}
          </FormattedMessage>

          <FormattedMessage
            id="home.filter.dealType.sale"
            defaultMessage={<option value="">Sale</option>}
          >
            {value => <option value="sale">{value}</option>}
          </FormattedMessage>
          <FormattedMessage
            id="home.filter.dealType.buy"
            defaultMessage={<option value="buy">Buy</option>}
          >
            {value => <option value="buy">{value}</option>}
          </FormattedMessage>
          <FormattedMessage
            id="home.filter.dealType.change"
            defaultMessage={<option value="change">Change</option>}
          >
            {value => <option value="change">{value}</option>}
          </FormattedMessage>
        </select>
      </div>
    );
  }
}

export default DealType;
