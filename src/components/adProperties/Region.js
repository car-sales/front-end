import React, { Component } from "react";
import { FormattedMessage } from "react-intl";

class Region extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div className="input-option">
        <FormattedMessage id="home.filter.region" defaultMessage="Region" />
        :&nbsp;
        {this.props.regions.length < 1 ||
        this.props.countrySelectedId === "" ? (
          <select required className="disabled-select" disabled>
            <FormattedMessage
              id="ad.select"
              defaultMessage={<option value="">Select</option>}
            >
              {value => <option value="">{value}</option>}
            </FormattedMessage>
          </select>
        ) : (
          <select required onChange={this.props.regionHandle}>
            <FormattedMessage
              id="ad.select"
              defaultMessage={<option value="">Select</option>}
            >
              {value => <option value="">{value}</option>}
            </FormattedMessage>
            {this.getRegions(this.props.regions)}
          </select>
        )}
      </div>
    );
  }

  getRegions(regions) {
    var regionsList = [];
    // console.log(models);
    regions.map(region => {
      regionsList.push(<option value={region.id}>{region.regionName}</option>);
    });
    return regionsList;
  }
}

export default Region;
