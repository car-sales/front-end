import React, { Component } from "react";
import { FormattedMessage } from "react-intl";

class Phone extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div className="input-option">
        <FormattedMessage id="ad.phone" defaultMessage="Phone" />
        :&nbsp;
        <input onChange={this.props.phoneHandle} type="number" required />
      </div>
    );
  }
}

export default Phone;
