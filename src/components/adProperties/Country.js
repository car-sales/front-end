import React, { Component } from "react";
import { FormattedMessage } from "react-intl";

class Country extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    let countries = this.props.countries;

    return (
      <div className="input-option">
        <FormattedMessage id="home.filter.countries" defaultMessage="Country" />
        :&nbsp;
        <select required onChange={this.props.countryHandle}>
          <FormattedMessage
            id="ad.select"
            defaultMessage={<option value="">Select</option>}
          >
            {value => <option value="">{value}</option>}
          </FormattedMessage>
          {countries.map(country => (
            <option value={country.id}>{country.countryName}</option>
          ))}
        </select>
      </div>
    );
  }
}

export default Country;
