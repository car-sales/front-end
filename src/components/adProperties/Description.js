import React, { Component } from "react";
import { FormattedMessage } from "react-intl";

class Description extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div className="input-option">
        <div>
          <FormattedMessage id="ad.description" defaultMessage="Description" />
          :&nbsp;
        </div>
        <textarea onChange={this.props.descriptionHandle} rows="10" cols="50" />
      </div>
    );
  }
}

export default Description;
