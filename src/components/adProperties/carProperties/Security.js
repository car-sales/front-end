import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import { toLowerCaseV2 } from "../../../Util";

class Security extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return this.securityContainer();
  }

  securityContainer() {
    var c = [];
    var i = 1;
    c.push(
      <div className="block-title">
        <FormattedMessage
          id="home.filter.securities"
          defaultMessage="Securities"
        />
      </div>
    );
    c.push(
      <div className="block-container">
        <div className="security-options">
          {this.props.securities.map(security => (
            <div className="input-option">
              <input
                onChange={this.props.securitiesHandle}
                value={security.id}
                type="checkbox"
                id={"security" + i}
              />
              <label for={"security" + i++}>
                &nbsp;
                <FormattedMessage
                  id={
                    "home.filter.securities." +
                    toLowerCaseV2(security.securityName)
                  }
                  defaultMessage={security.securityName}
                />
              </label>
            </div>
          ))}
        </div>
      </div>
    );

    return c;
  }
}

export default Security;
