import React, { Component } from "react";
import { FormattedMessage } from "react-intl";

class Mileage extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div className="input-option mileage-option">
        <div>
          <FormattedMessage id="home.filter.mileage" defaultMessage="Mileage" />
          :&nbsp;
        </div>
        <div>
          <input
            disabled={
              !this.props.modelSelected || this.props.chosenBrandId === 0
            }
            onChange={this.props.mileageHandle}
            className="number-input"
            placeholder="ex: 10000"
            type="number"
          />
        </div>
      </div>
    );
  }
}

export default Mileage;
