import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import { toLowerCaseV2 } from "../../../Util";

class Color extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return this.load();
  }

  load() {
    return [
      <div className="input-option">
        {this.props.colorType === "Interior color" ? (
          <FormattedMessage
            id="home.filter.interiorColors"
            defaultMessage="Interior color"
          />
        ) : (
          <FormattedMessage
            id="home.filter.exteriorColors"
            defaultMessage="Exterior color"
          />
        )}
        <select
          onChange={this.props.colorHandle}
          disabled={!this.props.modelSelected || this.props.chosenBrandId === 0}
        >
          <FormattedMessage
            id="ad.select"
            defaultMessage={<option value="">Select</option>}
          >
            {value => <option value="">{value}</option>}
          </FormattedMessage>
          {this.props.colors.map(color => (
            <FormattedMessage
              id={"home.filter.colors." + toLowerCaseV2(color)}
              defaultMessage={<option value={color}>{color}</option>}
            >
              {value => <option value={color}>{value}</option>}
            </FormattedMessage>
          ))}
        </select>
      </div>
    ];
  }
}

export default Color;
