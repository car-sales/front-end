import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import { toLowerCaseV2 } from "../../../Util";

class Traction extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div className="input-option">
        <FormattedMessage
          id="home.filter.tractions"
          defaultMessage="Traction"
        />
        :
        {!this.props.modelSelected || this.props.chosenBrandId === "" ? (
          <select className="disabled-select" disabled>
            <FormattedMessage
              id="ad.select"
              defaultMessage={<option value="">Select</option>}
            >
              {value => <option value="">{value}</option>}
            </FormattedMessage>{" "}
          </select>
        ) : (
          <select onChange={this.props.tractionHandle}>
            {this.getTraction()}
          </select>
        )}
      </div>
    );
  }

  getTraction() {
    var tractionList = [];
    tractionList.push(
      <FormattedMessage
        id="ad.select"
        defaultMessage={<option value="">Select</option>}
      >
        {value => <option value="">{value}</option>}
      </FormattedMessage>
    );
    this.props.traction.map(traction => {
      tractionList.push(
        <FormattedMessage
          id={"home.filter.tractions." + toLowerCaseV2(traction)}
          defaultMessage={<option value={traction}>{traction}</option>}
        >
          {value => <option value={traction}>{value}</option>}
        </FormattedMessage>
      );
    });
    return tractionList;
  }
}

export default Traction;
