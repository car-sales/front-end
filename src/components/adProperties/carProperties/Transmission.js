import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import { toLowerCaseV2 } from "../../../Util";

class Transmission extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div className="input-option">
        <FormattedMessage
          id="home.filter.transmissions"
          defaultMessage="Transmission"
        />
        :
        {!this.props.modelSelected || this.props.chosenBrandId === "" ? (
          <select className="disabled-select" disabled>
            <FormattedMessage
              id="ad.select"
              defaultMessage={<option value="">Select</option>}
            >
              {value => <option value="">{value}</option>}
            </FormattedMessage>{" "}
          </select>
        ) : (
          <select onChange={this.props.transmissionHandle}>
            {this.getTransmissions()}
          </select>
        )}
      </div>
    );
  }

  getTransmissions() {
    var transmissions = [];
    transmissions.push(
      <FormattedMessage
        id="ad.select"
        defaultMessage={<option value="">Select</option>}
      >
        {value => <option value="">{value}</option>}
      </FormattedMessage>
    );
    this.props.transmissions.map(transmission => {
      transmissions.push(
        <FormattedMessage
          id={"home.filter.transmissions." + toLowerCaseV2(transmission)}
          defaultMessage={<option value={transmission}>{transmission}</option>}
        >
          {value => <option value={transmission}>{value}</option>}
        </FormattedMessage>
      );
    });
    return transmissions;
  }
}

export default Transmission;
