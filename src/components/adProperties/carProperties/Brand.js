import React, { Component } from "react";
import { FormattedMessage } from "react-intl";

class Brand extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    let brands = this.props.brands;
    return (
      <div className="input-option">
        <FormattedMessage id="home.filter.brands" defaultMessage="Brand" />
        <select required onChange={this.props.brandHandle}>
          <FormattedMessage
            id="ad.select"
            defaultMessage={<option value="">Select</option>}
          >
            {value => <option value="">{value}</option>}
          </FormattedMessage>{" "}
          {brands.map(brand => (
            <option value={brand.id}>{brand.brandName}</option>
          ))}
        </select>
      </div>
    );
  }
}

export default Brand;
