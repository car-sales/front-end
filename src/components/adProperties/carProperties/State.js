import React, { Component } from "react";
import { FormattedMessage } from "react-intl";

class State extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div className="input-option">
        <FormattedMessage id="home.filter.state" defaultMessage="State" />:
        {!this.props.modelSelected || this.props.chosenBrandId === "" ? (
          <select className="disabled-select" disabled>
            <option value="">Select</option>
          </select>
        ) : (
          <select onChange={this.props.stateHandle}>
            <FormattedMessage
              id="ad.select"
              defaultMessage={<option value="">Select</option>}
            >
              {value => <option value="">{value}</option>}
            </FormattedMessage>
            <FormattedMessage
              id="home.filter.state.new"
              defaultMessage={<option value="new">New</option>}
            >
              {value => <option value="new">{value}</option>}
            </FormattedMessage>
            <FormattedMessage
              id="home.filter.state.used"
              defaultMessage={<option value="used">Used</option>}
            >
              {value => <option value="used">{value}</option>}
            </FormattedMessage>
            <FormattedMessage
              id="home.filter.state.needs_repair"
              defaultMessage={<option value="">Needs repair</option>}
            >
              {value => <option value="needs_repair">{value}</option>}
            </FormattedMessage>
          </select>
        )}
      </div>
    );
  }
}

export default State;
