import React, { Component } from "react";
import { FormattedMessage } from "react-intl";

class SteerWheelSide extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div className="input-option">
        <FormattedMessage
          id="home.filter.steerWheels"
          defaultMessage="Steer wheel"
        />
        :
        {!this.props.modelSelected || this.props.chosenBrandId === 0 ? (
          <select className="disabled-select" disabled>
            <FormattedMessage
              id="ad.select"
              defaultMessage={<option value="">Select</option>}
            >
              {value => <option value="">{value}</option>}
            </FormattedMessage>
          </select>
        ) : (
          <select onChange={this.props.steerWheelSideHandle}>
            <FormattedMessage
              id="ad.select"
              defaultMessage={<option value="">Select</option>}
            >
              {value => <option value="">{value}</option>}
            </FormattedMessage>
            <FormattedMessage
              id="home.filter.steerWheels.left"
              defaultMessage={<option value="left">Left</option>}
            >
              {value => <option value="left">{value}</option>}
            </FormattedMessage>
            <FormattedMessage
              id="home.filter.steerWheels.right"
              defaultMessage={<option value="right">Right</option>}
            >
              {value => <option value="right">{value}</option>}
            </FormattedMessage>
          </select>
        )}
      </div>
    );
  }
}

export default SteerWheelSide;
