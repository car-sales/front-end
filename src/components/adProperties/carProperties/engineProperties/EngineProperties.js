import React, { Component } from "react";
import Fuel from "./Fuel";
import Capacity from "./Capacity";
import HorsePower from "./HorsePower";
import $ from "jquery";
import { FormattedMessage } from "react-intl";

class EngineProperties extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    $(document).ready(function() {
      $(".block-title").click(function(event) {
        $(this)
          .toggleClass("block-active")
          .next()
          .slideToggle(150);
      });
    });
  }

  render() {
    return this.engineContainer();
  }

  engineContainer() {
    var c = [];

    c.push(
      <div className="block-title">
        <FormattedMessage
          id="ad.engineProperties"
          defaultMessage="Engine properties"
        />
      </div>
    );
    c.push(
      <div className="block-container">
        <div className="engine-options">
          <Fuel fuelHandle={this.props.fuelHandle} fuels={this.props.fuels} />
          <Capacity capacityHandle={this.props.capacityHandle} />
          <HorsePower horsePowerHandle={this.props.horsePowerHandle} />
        </div>
      </div>
    );

    return c;
  }
}

export default EngineProperties;
