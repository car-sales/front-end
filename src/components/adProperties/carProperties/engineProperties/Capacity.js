import React, { Component } from "react";
import { FormattedMessage } from "react-intl";

class Capacity extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div className="input-option">
        <div>
          <FormattedMessage
            id="home.filter.capacity"
            defaultMessage="Capacity"
          />
          :
        </div>
        <div>
          <input
            onChange={this.props.capacityHandle}
            className="number-input"
            placeholder="ex: 2.5"
            type="number"
            step="0.1"
            min="0.1"
            max="10"
          />
        </div>
      </div>
    );
  }
}

export default Capacity;
