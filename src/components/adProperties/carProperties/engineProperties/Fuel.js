import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import { toLowerCaseV2 } from "../../../../Util";

class Fuel extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div className="input-option">
        <div>
          <FormattedMessage id="home.filter.fuels" defaultMessage="Fuel" />:
        </div>
        <div>
          <select onChange={this.props.fuelHandle}>
            <FormattedMessage
              id="ad.select"
              defaultMessage={<option value="">Select</option>}
            >
              {value => <option value="">{value}</option>}
            </FormattedMessage>
            {this.props.fuels.map(fuel => (
              <FormattedMessage
                id={"home.filter.fuels." + toLowerCaseV2(fuel)}
                defaultMessage={<option value={fuel}>{fuel}</option>}
              >
                {value => <option value={fuel}>{value}</option>}
              </FormattedMessage>
            ))}
          </select>
        </div>
      </div>
    );
  }
}

export default Fuel;
