import React, { Component } from "react";
import { FormattedMessage } from "react-intl";

class HorsePower extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div className="input-option">
        <div>
          <FormattedMessage
            id="home.filter.horsePower"
            defaultMessage="Horse power"
          />
          :
        </div>
        <div>
          <input
            onChange={this.props.horsePowerHandle}
            className="number-input"
            placeholder="ex: 200"
            type="number"
          />
        </div>
      </div>
    );
  }
}

export default HorsePower;
