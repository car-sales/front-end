import React, { Component } from "react";
import { FormattedMessage } from "react-intl";

class Year extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div className="input-option">
        <FormattedMessage id="home.filter.year" defaultMessage="Year" />:
        <div>
          <input
            disabled={
              !this.props.modelSelected || this.props.chosenBrandId === 0
            }
            className="number-input"
            onChange={this.props.yearHandle}
            type="number"
            min="1900"
            max={new Date().getFullYear() + 1}
          />
        </div>
      </div>
    );
  }
}

export default Year;
