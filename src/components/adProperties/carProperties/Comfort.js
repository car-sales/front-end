import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import { toLowerCaseV2 } from "../../../Util";

class Comfort extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return this.comfortsContainer();
  }

  comfortsContainer() {
    var c = [];
    var i = 1;

    c.push(
      <div className="block-title">
        <FormattedMessage id="home.filter.comforts" defaultMessage="Comforts" />
      </div>
    );
    c.push(
      <div className="block-container">
        <div className="comfort-options">
          {this.props.comforts.map(comfort => (
            <div className="input-option">
              <input
                onChange={this.props.comfortsHandle}
                value={comfort.id}
                type="checkbox"
                id={"comfort" + i}
              />
              <label for={"comfort" + i++}>
                &nbsp;
                <FormattedMessage
                  id={
                    "home.filter.comforts." + toLowerCaseV2(comfort.comfortName)
                  }
                  defaultMessage={comfort.comfortName}
                />
              </label>
            </div>
          ))}
        </div>
      </div>
    );

    return c;
  }
}

export default Comfort;
