import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import { toLowerCaseV2 } from "../../../Util";

class BodyStyle extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div className="input-option">
        <FormattedMessage
          id="home.filter.bodyStyles"
          defaultMessage="Body style"
        />
        :
        {!this.props.modelSelected || this.props.chosenBrandId === "" ? (
          <select required className="disabled-select" disabled>
            <option value="">Select</option>
          </select>
        ) : (
          <select onChange={this.props.bodyStyleHandle}>
            {this.getBodyStyles()}
          </select>
        )}
      </div>
    );
  }

  getBodyStyles() {
    var bodyStyles = [];
    bodyStyles.push(
      <FormattedMessage
        id="ad.select"
        defaultMessage={<option value="">Select</option>}
      >
        {value => <option value="">{value}</option>}
      </FormattedMessage>
    );
    this.props.bodyStyles.map(bodyStyle => {
      bodyStyles.push(
        <FormattedMessage
          id={"home.filter.bodyStyles." + toLowerCaseV2(bodyStyle)}
          defaultMessage={<option value={bodyStyle}>{bodyStyle}</option>}
        >
          {value => <option value={bodyStyle}>{value}</option>}
        </FormattedMessage>
      );
    });
    return bodyStyles;
  }
}

export default BodyStyle;
