import React, { Component } from "react";
import { FormattedMessage } from "react-intl";

class DoorsNumber extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div className="input-option">
        <FormattedMessage
          id="home.filter.doorsNumber"
          defaultMessage="Doors number"
        />
        :&nbsp;
        {!this.props.modelSelected || this.props.chosenBrandId === "" ? (
          <select className="disabled-select" disabled>
            <FormattedMessage
              id="ad.select"
              defaultMessage={<option value="">Select</option>}
            >
              {value => <option value="">{value}</option>}
            </FormattedMessage>
          </select>
        ) : (
          <select onChange={this.props.doorsNumberHandle}>
            {this.getDoorsNumbers(5)}
          </select>
        )}
      </div>
    );
  }

  getDoorsNumbers(doors) {
    let doorsNumbers = [];
    doorsNumbers.push(
      <FormattedMessage
        id="ad.select"
        defaultMessage={<option value="">Select</option>}
      >
        {value => <option value="">{value}</option>}
      </FormattedMessage>
    );
    for (let i = 2; i <= doors; i++) {
      doorsNumbers.push(<option value={i}>{i}</option>);
    }
    return doorsNumbers;
  }
}

export default DoorsNumber;
