import React, { Component } from "react";
import { FormattedMessage } from "react-intl";

class Model extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div className="input-option">
        Model:&nbsp;
        {this.props.models.length < 1 || this.props.chosenBrandId === "" ? (
          <select required className="disabled-select" disabled>
            <FormattedMessage
              id="ad.select"
              defaultMessage={<option value="">Select</option>}
            >
              {value => <option value="">{value}</option>}
            </FormattedMessage>
          </select>
        ) : (
          <select required onChange={this.props.modelHandle}>
            <FormattedMessage
              id="ad.select"
              defaultMessage={<option value="">Select</option>}
            >
              {value => <option value="">{value}</option>}
            </FormattedMessage>
            {this.getModels(this.props.models)}
          </select>
        )}
      </div>
    );
  }

  getModels(models) {
    var modelOptions = [];
    // console.log(models);
    models.map(model => {
      modelOptions.push(<option value={model.id}>{model.modelName}</option>);
    });
    return modelOptions;
  }
}

export default Model;
