import React, { Component } from "react";
import axios from "axios";
import { API_URL } from "../Constants";

class Contact extends Component {
  constructor(props) {
    super(props);
    this.state = {
      result: ""
    };
  }

  componentDidMount() {
    if (this.props.match.params.id != null) {
      axios
        .get(`${API_URL}activate/${this.props.match.params.id}`)
        .then(Response => {
          this.setState(
            {
              result: Response.data
            },
            function() {
              window.location.href = "/login";
            }
          );
        })
        .catch(error => {
          if (error.response) {
            window.location.href = "/";
          }
        });
    } else {
      this.setState({
        result: "Check your email message to activate your account."
      });
    }
  }

  render() {
    return <div></div>;
  }
}

export default Contact;
