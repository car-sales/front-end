import React from "react";

export const Alert = ({ text }) => (
  <div className="alert-container">{text}</div>
);
