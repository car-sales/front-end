import React, { Component } from "react";
import axios from "axios";
import { API_URL } from "../Constants";
import { setCookie, getCookie } from "../CookieManager";
import { FormattedMessage } from "react-intl";
import Ads from "./Ads";

class UserProfile extends Component {
  constructor(props) {
    super(props);

    this.state = {
      user: null,
    };
  }

  componentDidMount() {
    this.setState({
      user: JSON.parse(localStorage.getItem("user")),
    });
  }

  deleteAd(id) {
    axios
      .delete(`${API_URL}ads/${id}`, {
        headers: {
          Authorization: getCookie("access_token"),
        },
      })
      .then((Response) => {
        axios
          .get(`${API_URL}current-user`, {
            params: { token: getCookie("access_token") },
            headers: {
              Authorization: getCookie("access_token"),
            },
          })
          .then((Response) => {
            const userData = Response.data;
            localStorage.setItem("user", JSON.stringify(userData));

            window.location.reload();
          })
          .catch((error) => {
            if (error.response) {
            }
          });
      })
      .catch((error) => {});
  }

  unlikeAd(id) {
    axios
      .get(`${API_URL}unlike/ad/${id}`, {
        params: { token: getCookie("access_token") },
        headers: {
          Authorization: getCookie("access_token"),
        },
      })
      .then((Response) => {
        axios
          .get(`${API_URL}current-user`, {
            params: { token: getCookie("access_token") },
            headers: {
              Authorization: getCookie("access_token"),
            },
          })
          .then((Response) => {
            const userData = Response.data;
            localStorage.setItem("user", JSON.stringify(userData));
            window.location.reload();
          })
          .catch((error) => {
            if (error.response) {
            }
          });
      })
      .catch((error) => {});
  }
  render() {
    return (
      <div className="profile">
        {this.state.user != null ? (
          <div>
            <h1 className="profile-text">
              <FormattedMessage id="user.profile" defaultMessage="Profile" />
            </h1>
            <ul className="user-info">
              <li>
                <FormattedMessage
                  id="user.userName"
                  defaultMessage="User name"
                />
                : <span className="user-name">{this.state.user.userName}</span>
              </li>
              <li>
                <FormattedMessage
                  id="user.firstName"
                  defaultMessage="First name"
                />
                : <span>{this.state.user.firstName}</span>
              </li>
              <li>
                <FormattedMessage
                  id="user.lastName"
                  defaultMessage="Last name"
                />
                : <span>{this.state.user.lastName}</span>
              </li>
              <li>
                <span>
                  {" "}
                  <FormattedMessage id="user.email" defaultMessage="Email" />:
                </span>{" "}
                <span>{this.state.user.email}</span>
              </li>
              <li>
                <FormattedMessage id="user.phone" defaultMessage="Phone" />:{" "}
                <span>{this.state.user.phone}</span>
              </li>
              <li>
                <FormattedMessage id="user.created" defaultMessage="Created" />:{" "}
                <span>{this.state.user.createdAt}</span>
              </li>
            </ul>
            <ul className="user-ads">
              {this.state.user.userAdsDto.length > 0 ? (
                <li>
                  <div>
                    <FormattedMessage id="user.myAds" defaultMessage="My ads" />
                    :
                  </div>
                  <Ads
                    ads={this.state.user.userAdsDto}
                    category="userAds"
                    delete={this.deleteAd}
                  />
                </li>
              ) : (
                ""
              )}
              {this.state.user.likedAdsDto.length > 0 ? (
                <li>
                  <div>
                    <FormattedMessage
                      id="user.savedAds"
                      defaultMessage="Saved ads"
                    />
                    :
                  </div>
                  <Ads
                    ads={this.state.user.likedAdsDto}
                    category="savedAds"
                    unlikeAd={this.unlikeAd}
                  />
                </li>
              ) : (
                ""
              )}
            </ul>
          </div>
        ) : (
          <center>
            <img src="/loading-bubbles.gif" width="10%" />
          </center>
        )}
      </div>
    );
  }
}

export default UserProfile;
