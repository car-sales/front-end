import axios from "axios";
import { API_URL } from "./Constants";

export let get = route => axios.get(`${API_URL}${route}`);
