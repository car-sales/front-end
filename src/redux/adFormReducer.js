import { OPTION_ENABLED, OPTION_DISABLED } from "./types";

const initialState = {};

export const adFormReducer = (state = initialState, action) => {
  switch (action.type) {
    case OPTION_ENABLED:
      return { ...state, [action.payload]: true };
    case OPTION_DISABLED:
      return { ...state, [action.payload]: null };
    default:
      return state;
  }

  return state;
};
