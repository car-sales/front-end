import {
  SIGN_UP,
  SHOW_SIGNUP_ERROR,
  HIDE_SIGNUP_ERROR,
  SHOW_SIGNUP_SUCCESS,
  HIDE_SIGNUP_SUCCESS
} from "./types";

const initialState = {};

export const registerReducer = (state = initialState, action) => {
  switch (action.type) {
    case SIGN_UP:
      return action.payload;
    case SHOW_SIGNUP_ERROR:
      return { ...state, signup_error: true };
    case HIDE_SIGNUP_ERROR:
      return { ...state, signup_error: false };
    case SHOW_SIGNUP_SUCCESS:
      return { ...state, signup_success: true };
    case HIDE_SIGNUP_SUCCESS:
      return { ...state, signup_success: false };
    default:
      return state;
  }

  return state;
};
