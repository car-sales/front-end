import { ADD_FILTER } from "./types";

const initialState = {
  //   priceStart: 0,
  //   priceEnd: Math.pow(10, 7),
  //   yearStart: 1900,
  //   yearEnd: new Date().getFullYear() + 1,
  //   horsePowerStart: 10,
  //   horsePowerEnd: 10000,
  //   capacityStart: 0.5,
  //   capacityEnd: 10,
  //   mileageStart: 0,
  //   mileageEnd: 1000000,
  //   currency: "eur",
  //   brands: [],
  //   models: [],
  //   fuels: [],
  //   bodyStyles: [],
  //   tractions: [],
  //   transmissions: [],
  //   interiorColors: [],
  //   exteriorColors: [],
  //   doorsNumbers: [],
  //   comforts: [],
  //   securities: [],
  //   states: [],
  //   steerWheelSides: [],
  //   dealTypes: [],
  //   countries: [],
  //   regions: [],
  //   negotiable: "true",
  //   isFiltered: false
};

export const filterReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_FILTER:
      return action.payload;
    default:
      return state;
  }

  return state;
};
