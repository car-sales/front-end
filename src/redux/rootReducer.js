import { combineReducers } from "redux";
import { registerReducer } from "./registerReducer";
import { adsReducer } from "./adsReducer";
import { appReducer } from "./appReducer";
import { filterReducer } from "./filterReducer";
import { adFormReducer } from "./adFormReducer";

export const rootReducer = combineReducers({
  ads: adsReducer,
  register: registerReducer,
  app: appReducer,
  filter: filterReducer,
  adForm: adFormReducer
});
