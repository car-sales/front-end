import { CREATE_AD, FETCH_ADS } from "./types";

const initialState = { adsAddedToday: -1 };

export const adsReducer = (state = initialState, action) => {
  switch (action.type) {
    case CREATE_AD:
      return action.payload;
    case FETCH_ADS:
      return action.payload;
    default:
      return state;
  }

  return state;
};
