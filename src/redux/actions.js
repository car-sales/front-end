import {
  CREATE_AD,
  FETCH_ADS,
  SHOW_LOADER,
  HIDE_LOADER,
  SHOW_ALERT,
  HIDE_ALERT,
  HIDE_SIGNUP_ERROR,
  SHOW_SIGNUP_ERROR,
  SHOW_SIGNUP_SUCCESS,
  HIDE_SIGNUP_SUCCESS,
  SHOW_DISABLED,
  HIDE_DISABLED,
  OPTION_ENABLED,
  OPTION_DISABLED,
} from "./types";
import Axios from "axios";
import { API_URL } from "../Constants";
import { setCookie } from "../CookieManager";

export function showLoader() {
  return {
    type: SHOW_LOADER,
  };
}

export function hideLoader() {
  return {
    type: HIDE_LOADER,
  };
}

export function showAlert(text) {
  return (dispatch) => {
    dispatch({ type: SHOW_ALERT, payload: text });

    setTimeout(() => {
      dispatch(hideAlert());
    }, 5000);
  };
}

export function hideAlert() {
  return {
    type: HIDE_ALERT,
  };
}

export function showSignupError() {
  return {
    type: SHOW_SIGNUP_ERROR,
  };
}

export function hideSignupError() {
  return {
    type: HIDE_SIGNUP_ERROR,
  };
}

export function showSignupSuccess() {
  return {
    type: SHOW_SIGNUP_SUCCESS,
  };
}

export function hideSignupSuccess() {
  return {
    type: HIDE_SIGNUP_SUCCESS,
  };
}

export function showDisabled() {
  return {
    type: SHOW_DISABLED,
  };
}

export function hideDisabled() {
  return {
    type: HIDE_DISABLED,
  };
}

export function signUp(user) {
  return async (dispatch) => {
    try {
      dispatch(showDisabled());
      const response = await Axios.post(API_URL + "sign-up", user);
      await response.data;
      dispatch(hideDisabled());
      dispatch(showSignupSuccess());
    } catch (e) {
      dispatch(hideDisabled());
      dispatch(showSignupError());
      dispatch(showAlert("Error"));
    }
  };
}

export function login(credentials) {
  return async (dispatch) => {
    try {
      dispatch(showDisabled());
      const response = await Axios.post(API_URL + "login", credentials);
      const authorization = await response.data.Authorization;
      setCookie("access_token", authorization);
      await Axios.get(`${API_URL}current-user`, {
        params: { token: authorization },
        headers: {
          Authorization: authorization,
        },
      }).then((Response) => {
        const userData = Response.data;
        localStorage.setItem("user", JSON.stringify(userData));
        window.location.href = "/";
      });

      setTimeout(() => {
        dispatch(hideDisabled());
      }, 1000);
    } catch (e) {
      dispatch(hideDisabled());
      dispatch(showAlert("Bad credentials"));
    }
  };
}

export function createAd(ad) {
  return {
    type: CREATE_AD,
    payload: ad,
  };
}

export function fetchAds(url) {
  return async (dispatch) => {
    dispatch(showLoader());

    const response = await Axios.get(API_URL + url);
    const json = await response.data;
    dispatch({ type: FETCH_ADS, payload: json });

    dispatch(hideLoader());
  };
}

export function optionEnable(op) {
  return { type: OPTION_ENABLED, payload: op };
}

export function optionDisable(op) {
  return { type: OPTION_DISABLED, payload: op };
}
