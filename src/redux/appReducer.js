import {
  SHOW_LOADER,
  HIDE_LOADER,
  SHOW_ALERT,
  HIDE_ALERT,
  SHOW_DISABLED,
  HIDE_DISABLED,
  LOGIN
} from "./types";

const initilState = {
  loading: true
};

export const appReducer = (state = initilState, action) => {
  switch (action.type) {
    case SHOW_LOADER:
      return { ...state, loading: true };
    case HIDE_LOADER:
      return { ...state, loading: false };
    case SHOW_ALERT:
      return { ...state, alert: action.payload };
    case HIDE_ALERT:
      return { ...state, alert: null };
    case SHOW_DISABLED:
      return { ...state, disabled: true };
    case HIDE_DISABLED:
      return { ...state, disabled: false };
    case LOGIN:
      return { ...state };

    default:
      return state;
  }
};
