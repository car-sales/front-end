import React, { Component } from "react";
import "./App.css";
import Contact from "./components/Contact";
import AdPage from "./components/AdPage";
import Home from "./components/Home";
import UserProfile from "./components/UserProfile";
import Login from "./components/Login";
import Register from "./components/Register";
import AddAnAd from "./components/AddAnAd";
import Activation from "./components/Activation";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Header from "./components/Header";
import $ from "jquery";
import { IntlProvider } from "react-intl";
import en from "react-intl/locale-data/en";
import ro from "react-intl/locale-data/ro";
import ru from "react-intl/locale-data/ru";
import { addLocaleData } from "react-intl";
import { roLang } from "./LocaleLang";
import Loader from "./components/Loader";
import { connect } from "react-redux";
import Error from "./components/alerts/Error";
import Success from "./components/alerts/Success";

addLocaleData(en);
addLocaleData(ro);
addLocaleData(ru);

class App extends Component {
  constructor() {
    super();

    this.state = {
      locale: "ro",
      messages:
        localStorage.getItem("language") != null
          ? JSON.parse(localStorage.getItem("language")).messages
          : roLang.messages
    };
  }

  componentDidMount() {
    $(window).scroll(function() {
      var scroll = $(window).scrollTop();

      if (scroll >= 20) {
        $(".header-body").addClass("header-body-smaller");
        $(".header-logo").addClass("header-logo-smaller");
        $(".header-link").addClass("header-link-smaller");
      } else {
        $(".header-body").removeClass("header-body-smaller");
        $(".header-logo").removeClass("header-logo-smaller");
        $(".header-link").removeClass("header-link-smaller");
      }
    });

    $(document).ready(function() {
      $("header .menu-toggle").click(function(event) {
        $("nav").toggleClass("active");
        $(".menu-toggle").toggleClass("active");
        $("body,html").toggleClass("lock");
      });

      // if ($(".menu-toggle").css("display") == "block") {
      $(".header-link").click(function() {
        $("body,html").removeClass("lock");
        $(".menu-toggle").removeClass("active");
        $("nav").addClass("active");
      });
      // }

      // $("ul li").hover(function() {
      //   $(this)
      //     .siblings()
      //     .removeClass("active");
      //   $(this).toggleClass("active");
      // });

      $(".header-burger").click(function(event) {
        $(".header-burger,.header-menu").toggleClass("active");
        $("body,html").toggleClass("lock");
      });
    });
  }

  getLanguage = event => {
    this.setState({
      locale: JSON.parse(localStorage.getItem("language")).locale,
      messages: JSON.parse(localStorage.getItem("language")).messages
    });
  };

  render() {
    return (
      <div>
        {this.props.register.signup_error && (
          <Error message="Something went wrong" />
        )}
        {this.props.register.signup_success && (
          <Success message="Activate your account" />
        )}

        <IntlProvider locale={this.state.locale} messages={this.state.messages}>
          <Router>
            <Header
              getLanguage={this.getLanguage}
              hideLoader={this.hideLoader}
              displayLoader={this.displayLoader}
            />
            <Loader classes="" />

            <div className="content">
              <div className="container">
                <Switch>
                  <Route exact path="/contact" component={Contact} />
                  <Route exact path="/ad/:id" component={AdPage} />
                  <Route exact path="/login" component={Login} />
                  <Route exact path="/register" component={Register} />
                  <Route exact path="/user" component={UserProfile} />
                  <Route exact path="/ad" component={AddAnAd} />
                  <Route exact path="/" component={Home} />
                  <Route exact path="/activate/:id" component={Activation} />
                  <Route exact path="/activation" component={Activation} />
                </Switch>
                {/* <Route path='/' {404} /> */}
              </div>
              {/* <Footer /> */}
            </div>
          </Router>
        </IntlProvider>
      </div>
    );
  }

  displayLoader() {
    $(document).ready(function() {
      $(".loader").addClass("active");
    });
  }

  hideLoader() {
    $(document).ready(function() {
      $(".loader").removeClass("active");
    });
  }
}

const mapStateToProps = state => state;

export default connect(mapStateToProps, null)(App);
