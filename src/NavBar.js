import React,{Component} from 'react';
import {Link} from 'react-router-dom';
import logo from 'C:/Users/Ion/Desktop/myLogoUpdated.png';

class NavBar extends Component  {
    render() {
    return(
      <nav className="navbar navbar-expand-lg navbar-dark bg-nav">
      <Link className="navbar-brand" to="/"><img title="Home" src={logo} width="60" height="40" alt="Home"/></Link>
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon"></span>
      </button>
    
      <div className="collapse navbar-collapse float-right bg-nav" id="navbarSupportedContent">
        <ul className="navbar-nav ml-auto">
          <li className="nav-item active">
            <Link className="nav-link" to="/"><i className="fas fa-home"></i>&nbsp;Home<span className="sr-only">(current)</span></Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link" to="/contact"><i className="fas fa-phone"></i>&nbsp;Contact us</Link>
          </li>
          <li className="nav-item dropdown">
            <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i className="fas fa-user-alt"></i>&nbsp;My account 
            </a>
            <div className="dropdown-menu" aria-labelledby="navbarDropdown">
              <Link className="dropdown-item" to="/user">Profile</Link>
              <Link className="dropdown-item" to="/register">Sign-up</Link>
              <div className="dropdown-divider"></div>
              <Link className="dropdown-item" to="/form">Login</Link>
            </div>
          </li>
          
        </ul>
        <form className="form-inline my-2 my-lg-0">
          <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" />
          <button className="btn btn-outline-warning my-2 my-sm-0" type="submit">Search</button>
        </form>
      </div>
    </nav>
    );
    }
}
export default NavBar;