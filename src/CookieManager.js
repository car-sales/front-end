import Cookies from "universal-cookie";

export const setCookie = (name, data) => {
  const cookies = new Cookies();

  console.log(name);
  cookies.set(name, data, { maxAge: 3600 * 24 * 7 }, { path: "/" });
};

export const getCookie = name => {
  const cookies = new Cookies();
  return cookies.get(name);
};

export const logOut = () => {
  localStorage.removeItem("user");
  const cookies = new Cookies();

  cookies.remove("access_token");
};
