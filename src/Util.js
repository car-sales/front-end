import filtersObj from "./FiltersObj";

export const setData = (dataName, setState) => {
  if (filtersObj[dataName].length > 0)
    for (let i = 0; i < filtersObj[dataName].length; i++) {
      setState(filtersObj[dataName][i]);
    }
};

// export const checkValues = (valueStart,)

export const filterHandle = (dataListName, dataName, dataValue) => {
  let dataList = filtersObj[dataListName];
  // filtersObj.negotiable = false;
  if (dataValue) {
    dataList.push(dataName);
    filtersObj[dataListName] = dataList;
  } else {
    var idx = dataList.indexOf(dataName);
    if (idx !== -1) dataList.splice(idx, 1);

    filtersObj[dataListName] = dataList;
  }
};

const toLowerCase = (name) => {
  let n = name;
  n = n.replace("-", "_");
  n = n.replace(" ", "_");
  return n.toLowerCase();
};

export const toLowerCaseV2 = (name) => {
  let n = name;
  n = n.replace(/ /g, "_");
  return n.toLowerCase();
};
