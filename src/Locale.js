import { enLang, roLang, ruLang } from "./LocaleLang";

export const switchLang = locale => {
  switch (locale) {
    case "en": {
      localStorage.setItem("language", JSON.stringify(enLang));
      break;
    }
    case "ro": {
      localStorage.setItem("language", JSON.stringify(roLang));
      break;
    }
    case "ru": {
      localStorage.setItem("language", JSON.stringify(ruLang));
      break;
    }
  }
};

export const getRangeNames = () => {
  try {
    var from = JSON.parse(localStorage.getItem("language")).messages[
      "home.filter.from"
    ];
    var to = JSON.parse(localStorage.getItem("language")).messages[
      "home.filter.to"
    ];

    return { from: from, to: to };
  } catch (error) {
    return null;
  }
};
